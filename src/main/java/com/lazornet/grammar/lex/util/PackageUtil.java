package com.lazornet.grammar.lex.util;

import java.io.File;

public class PackageUtil {
    public static String getPackageDirectory(File outputDirectory, String packageName) {
        return outputDirectory.getAbsolutePath() + "/" + packageName.replace('.', '/');
    }
}
