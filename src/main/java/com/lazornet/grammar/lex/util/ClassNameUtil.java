package com.lazornet.grammar.lex.util;

public class ClassNameUtil {
    public static String classNameFor(String astNodeName) {
        if (Character.isUpperCase(astNodeName.charAt(0))) {
            return tokenNameToClassName(astNodeName);
        } else {
            return ruleNameToClassName(astNodeName);
        }
    }

    private static String ruleNameToClassName(String ruleName) {
        return ruleName.substring(0, 1).toUpperCase() + ruleName.substring(1);
    }

    private static String tokenNameToClassName(String tokenName) {
        final StringBuilder sb = new StringBuilder(tokenName.length());
        boolean nextUpper = true;
        for (char ch : tokenName.toCharArray()) {
            if (ch == '_') {
                nextUpper = true;
            } else {
                if (nextUpper) {
                    sb.append(ch);
                    nextUpper = false;
                } else {
                    sb.append(Character.toLowerCase(ch));
                }
            }
        }
        return sb.toString();
    }
}
