package com.lazornet.grammar.lex.util

fun escapeChar(ch: Char): String {
    return when (ch) {
        '\\' -> "\\\\"
        '\'' -> "\\'"
        '\t' -> "\\t"
        '\r' -> "\\r"
        '\n' -> "\\n"
        else -> ch.toString()
    }
}
