package com.lazornet.grammar.lex.util;

public class CharTools {
    private static final char[] ESC_BACKSLASH = {'\\', '\\'};
    private static final char[] ESC_SINGLE_QUOTE = {'\\', '\''};
    private static final char[] ESC_TAB = {'\\', 't'};
    private static final char[] ESC_CR = {'\\', 'r'};
    private static final char[] ESC_NEWLINE = {'\\', 'n'};

    public static char[] escape(char ch) {
        switch (ch) {
            case '\\':
                return ESC_BACKSLASH;
            case '\'':
                return ESC_SINGLE_QUOTE;
            case '\t':
                return ESC_TAB;
            case '\r':
                return ESC_CR;
            case '\n':
                return ESC_NEWLINE;
            default:
                return new char[] { ch };
        }
    }
}
