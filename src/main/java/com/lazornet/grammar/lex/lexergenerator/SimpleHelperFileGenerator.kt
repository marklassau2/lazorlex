package com.lazornet.grammar.lex.lexergenerator

import java.io.PrintWriter

fun generateLexerParseException(out: PrintWriter, packageName: String) {
    out.println("""
package $packageName;

public class LexerParseException extends RuntimeException {
    public LexerParseException(String message) {
        super(message);
    }
}
""".trimIndent())
}

fun generateLexingCharStream(out: PrintWriter, packageName: String) {
    out.println("""
package $packageName;

public class LexingCharStream {
    private final CharSequence text;
    private int tokenStart;
    private int idx;

    LexingCharStream(CharSequence buffer, int offset) {
        this.text = buffer;
        this.tokenStart = offset;
        this.idx = offset;
    }

    void rewind() {
        idx = tokenStart;
    }
    
    public char read() {
        if (idx >= text.length()) return (char) 0;
        return text.charAt(idx++);
    }

    int getTokenStart() {
        return tokenStart;
    }
    
    String getPosition() {
        if (text == null) return "" + tokenStart;
        int lineNumber = 1;
        int columnNumber = 1;
        int idx = 0;
        while (idx < tokenStart) {
            switch (text.charAt(idx)) {
                case '\n':
                    lineNumber++;
                    columnNumber = 0;
                    break;
                case '\r':
                    lineNumber++;
                    columnNumber = 0;
                    if (idx + 1 < tokenStart && text.charAt(idx + 1) == '\n') {
                        // CRLF - nibble one more char
                        idx++;
                    }
                    break;
                default:
                    columnNumber++;
                    break;
            }
            idx++;
        }
        columnNumber++;
        return "" + lineNumber + ':'+ columnNumber;
    }
    
    public void setTokenStart(int tokenStart) {
        this.tokenStart = tokenStart;
    }

    public int length() {
        return text.length();
    }

    CharSequence getTextBuffer() {
        return text;
    }
}
""".trimIndent())
}


