package com.lazornet.grammar.lex.lexergenerator

import com.lazornet.grammar.lex.lexercompiler.LexerDefinition
import java.io.PrintWriter
import java.lang.StringBuilder

class TokenClassGenerator(private val packageName: String, private val lexerName: String, private val lexerDefinition: LexerDefinition) {
    fun generate(out: PrintWriter) {
        out.println("""
package $packageName;

public class ${lexerName}Token {
    public enum Type {${tokenTypes()}}

    private final Type tokenType;
    private final int tokenStart;
    private final String text;

    public ${lexerName}Token(Type tokenType, int tokenStart, String text) {
        this.tokenType = tokenType;
        this.tokenStart = tokenStart;
        this.text = text;
    }

    public Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return tokenStart;
    }

    public String getText() {
        return text;
    }
}
""".trimIndent())
    }

    private fun tokenTypes(): String {
        val sb = StringBuilder()
        var columnIndex = "    public enum Type {".length
        var firstTokenType = true
        lexerDefinition.tokenTypes.forEach {
            if (firstTokenType) {
                firstTokenType = false
                columnIndex += it.length + 2
            } else {
                sb.append(',')
                if (columnIndex + it.length >= 120) {
                    // time for a new line
                    sb.append('\n')
                    sb.append("        ")
                    columnIndex = it.length + 10
                } else {
                    sb.append(' ')
                    columnIndex += it.length + 2
                }
            }
            sb.append(it)
        }
        return sb.toString()
    }
}