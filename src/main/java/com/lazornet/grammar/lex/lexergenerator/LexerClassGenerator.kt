package com.lazornet.grammar.lex.lexergenerator

import com.lazornet.grammar.lex.lexercompiler.LexerDefinition
import com.lazornet.grammar.lex.lexercompiler.RegexDefChunk.Quantifier.*
import com.lazornet.grammar.lex.util.escapeChar
import java.io.PrintWriter

class LexerClassGenerator(private val packageName: String, private val lexerName: String, private val lexerDefinition: LexerDefinition) {
    fun generate(out: PrintWriter) {
        out.println("""
package $packageName;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class ${lexerName}Lexer {
${generateLexerStates()}

    private LexingCharStream charStream;
    private int state = 0;
    private ${lexerName}Token.Type tokenType;
    private int tokenEnd = -1;

    public static List<${lexerName}Token> lexAllTokens(CharSequence text) {
        final List<${lexerName}Token> tokens = new LinkedList<>();
        ${lexerName}Lexer lexer = new ${lexerName}Lexer();
        lexer.start(text);
        while (lexer.getTokenType() != null) {
            if (!lexer.getTokenType().name().startsWith("_")) {
                tokens.add(new ${lexerName}Token(lexer.getTokenType(), lexer.getTokenStart(), lexer.getTokenText()));
            }
            lexer.advance();
        }
        return tokens;
    }

    public void start(CharSequence buffer) {
        reset(buffer, 0, 0);
    }

    public void reset(CharSequence buffer, int startOffset, int initialState) {
        this.charStream = new LexingCharStream(buffer, startOffset);
        this.state = initialState;
        this.tokenEnd = startOffset;

        readToken();
    }

    public void readToken() {
        if (tokenEnd >= charStream.length()) {
            tokenType = null;
            return;
        }

        int bestLength = 0;
        LexerRule bestMatch = null;
        for (LexerRule rule : states[state].getRules()) {
            charStream.rewind();
            int matchLength = rule.match(charStream);
            if (matchLength > bestLength) {
                bestLength = matchLength;
                bestMatch = rule;
            }
        }
        if (bestMatch == null) {
            throw new IllegalStateException("syntax error: no matching token found at " +
                    charStream.getPosition() + " (state: " + states[state].name + ")\n");
        }
        if (bestMatch.hasNextState()) {
            state = bestMatch.nextState;
        }
        tokenEnd = charStream.getTokenStart() + bestLength;
        tokenType = bestMatch.tokenType;
    }

    public void advance() {
        charStream.setTokenStart(tokenEnd);
        readToken();
    }

    public int getState() {
        return state;
    }

    public int getTokenEnd() {
        return tokenEnd;
    }

    public ${lexerName}Token.Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return charStream.getTokenStart();
    }

    public String getTokenText() {
        return charStream.getTextBuffer().subSequence(getTokenStart(), tokenEnd).toString();
    }

    private static class LexerState {
        private final String name;
        private final Iterable<LexerRule> rules;

        private LexerState(String name, Iterable<LexerRule> rules) {
            this.name = name;
            this.rules = rules;
        }

        private Iterable<LexerRule> getRules() {
            return rules;
        }
    }

    private static class LexerRule {
        private final TokenMatcher tokenMatcher;
        private final int nextState;
        private final ${lexerName}Token.Type tokenType;

        private LexerRule(TokenMatcher tokenMatcher, ${lexerName}Token.Type tokenType, int nextState) {
            this.tokenMatcher = tokenMatcher;
            this.tokenType = tokenType;
            this.nextState = nextState;
        }

        private int match(LexingCharStream charStream) {
            return tokenMatcher.match(charStream);
        }

        private boolean hasNextState() {
            return nextState >= 0;
        }
    }

    interface TokenMatcher {
        int match(LexingCharStream charStream);
    }
}
""".trimIndent())
    }

    private fun generateLexerStates(): String {
        val sb = StringBuilder()

        // build up the matchers
        for (rule in lexerDefinition.rules) {
                sb.append("    private static final TokenMatcher matcher${rule.index} = charStream -> {\n")
            if (rule.isExactText) {
                sb.append("        // Exact text '${rule.exactText}'\n")
                sb.append("        char ch;\n")
                for (c in rule.exactText) {
                    sb.append("""
                            |        ch = charStream.read();
                            |        if (ch != '${escapeChar(c)}') {
                            |            return -1;
                            |        }
                            |""".trimMargin())
                }
                sb.append("        return ${rule.exactText.length};\n")
            } else {
                sb.append("        char ch;\n")
                sb.append("        int count = 0;\n")
                sb.append("        ch = charStream.read();\n")
                val regexChunkIterator = rule.regexDef.chunks.iterator()
                while (regexChunkIterator.hasNext()) {
                    val regexChunk = regexChunkIterator.next()
                    when (regexChunk.quantifier) {
                        EXACTLY_ONE -> {
                            sb.append("        if (")
                            regexChunk.regexChar.generateMatchCode(sb)
                            sb.append(") {\n")
                            sb.append("            count++;\n")
                            if (regexChunkIterator.hasNext()) sb.append("            ch = charStream.read();\n")
                            sb.append("        } else {\n")
                            sb.append("            return -1;\n")
                            sb.append("        }\n")
                        }
                        MAYBE_ONE -> {
                            sb.append("        if (")
                            regexChunk.regexChar.generateMatchCode(sb)
                            sb.append(") {\n")
                            sb.append("            count++;\n")
                            if (regexChunkIterator.hasNext()) sb.append("            ch = charStream.read();\n")
                            sb.append("        }\n")
                        }
                        ZERO_OR_MORE -> {
                            sb.append("        while (")
                            regexChunk.regexChar.generateMatchCode(sb)
                            sb.append(") {\n")
                            sb.append("            count++;\n")
                            sb.append("            ch = charStream.read();\n")
                            sb.append("        }\n")
                        }
                        ONE_OR_MORE -> {
                            sb.append("        if (")
                            regexChunk.regexChar.generateMatchCode(sb)
                            sb.append(") {\n")
                            sb.append("            count++;\n")
                            sb.append("            ch = charStream.read();\n")
                            sb.append("        } else {\n")
                            sb.append("            return -1;\n")
                            sb.append("        }\n")
                            sb.append("        while (")
                            regexChunk.regexChar.generateMatchCode(sb)
                            sb.append(") {\n")
                            sb.append("            count++;\n")
                            sb.append("            ch = charStream.read();\n")
                            sb.append("        }\n")
                        }
                    }
                }
                sb.append("        return count;\n")
            }
            sb.append("    };\n")
        }
        sb.append("\n")
        // create the array of states which refer to matchers by name
        sb.append("    private static final LexerState[] states = {\n")

        for (state in lexerDefinition.states) {
            sb.append("            new LexerState(\"${state.name}\", asList(\n")
            var firstRule = true
            for (rule in state.rules) {
                if (firstRule) {
                    firstRule = false
                } else {
                    sb.append(",\n")
                }
                val nextStateIndex = lexerDefinition.stateIndex(rule.nextState)
                sb.append("                    new LexerRule(matcher${rule.index}, ${lexerName}Token.Type.${rule.tokenName}, $nextStateIndex)")
//                if (rule.isRegexPattern) {
//                    sb.append("                    rule(matcher$ruleIndex, ${lexerName}Token.Type.${rule.tokenName}, $nextStateIndex),\n")
//                } else {
//                    sb.append("                    textRule(\"${escapeChars(rule.pattern)}\", ${lexerName}Token.Type.${rule.tokenName}, $nextStateIndex),\n")
//                }
            }
            sb.append("\n")
            sb.append("            )),\n")
        }
        sb.append("    };")

        return sb.toString()


//        new LexerState("init", asList(
//        regexRule("[a-z][a-z_0-9]*", Token.Type.STATE, -1),
//        regexRule("{", Token.Type.L_BRACE, 1),
//        regexRule("[ \t\r\n]+", null)
//        )),
    }
}