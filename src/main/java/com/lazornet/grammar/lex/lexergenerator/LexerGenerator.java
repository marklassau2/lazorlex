package com.lazornet.grammar.lex.lexergenerator;

import com.lazornet.grammar.lazorlex.lex.LexLexer;
import com.lazornet.grammar.lazorlex.lex.LexParseException;
import com.lazornet.grammar.lazorlex.lex.LexParser;
import com.lazornet.grammar.lazorlex.lex.Statements;
import com.lazornet.grammar.lex.lexercompiler.LexerCompiler;
import com.lazornet.grammar.lex.lexercompiler.LexerDefinition;
import com.lazornet.grammar.lex.util.PackageUtil;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LexerGenerator {
    private final String packageDirectory;
    private final String packageName;
    private final File lexFile;
    private final Log log;
    private final String strippedFileName;

    public LexerGenerator(File outputDirectory, String packageName, File lexFile, Log log) {
        this.packageName = packageName;
        this.lexFile = lexFile;
        this.log = log;
        this.packageDirectory = PackageUtil.getPackageDirectory(outputDirectory, packageName);
        strippedFileName = stripFileName(lexFile);
    }

    public static String stripFileName(File lexFile) {
        int dotIndex = lexFile.getName().lastIndexOf('.');
        // TODO: Enforce uppercase on first char?
        return lexFile.getName().substring(0, dotIndex);
    }

    public void generateLexer() throws MojoExecutionException {
        final String text;
        try {
            text = readText(lexFile);
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to read the lex file " + lexFile.getName(), e);
        }
        final Statements statements;
        try {
            statements = LexParser.parse(LexLexer.lexAllTokens(text), text);
        } catch (LexParseException ex) {
            log.error("Parse error in " + lexFile.getName());
            log.error(ex.getMessage());
            throw new MojoExecutionException("Parse error in " + lexFile.getName() + ": " + ex.getMessage(), ex);
        }
        final LexerDefinition lexerDefinition = LexerCompiler.compile(statements);
        try {
            generateFiles(lexerDefinition);
        } catch (IOException e) {
            throw new MojoExecutionException("IO error trying to write generated files for " + lexFile.getName(), e);
        }
    }

    private void generateFiles(LexerDefinition lexerDefinition) throws IOException {
        // ensure parent directory exists
        //noinspection ResultOfMethodCallIgnored
        new File(packageDirectory).mkdirs();

        generateTokenClass(lexerDefinition);
        generateLexerClass(lexerDefinition);
        generateLexerParseException();
        generateLexingCharStream();
    }

    private void generateLexingCharStream() throws IOException {
        PrintWriter out = new PrintWriter(getFileWriter("LexingCharStream.java"));
        SimpleHelperFileGeneratorKt.generateLexingCharStream(out, packageName);
        out.close();
    }

    private void generateLexerParseException() throws IOException {
        PrintWriter out = new PrintWriter(getFileWriter("LexerParseException.java"));
        SimpleHelperFileGeneratorKt.generateLexerParseException(out, packageName);
        out.close();
    }

    private void generateLexerClass(LexerDefinition lexerDefinition) throws IOException {
        String className = strippedFileName + "Lexer";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new LexerClassGenerator(packageName, strippedFileName, lexerDefinition).generate(out);

        out.close();
    }

    private void generateTokenClass(LexerDefinition lexerDefinition) throws IOException {
        String className = strippedFileName + "Token";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new TokenClassGenerator(packageName, strippedFileName, lexerDefinition).generate(out);

        out.close();
    }

    private FileWriter getFileWriter(String className) throws IOException {
        return new FileWriter(new File(packageDirectory, className));
    }

    private String readText(File lexFile) throws IOException {
        StringBuilder sb = new StringBuilder();
        Files.lines( Paths.get(lexFile.getAbsolutePath()), StandardCharsets.UTF_8)
            .forEach(s -> sb.append(s).append("\n"));
        return sb.toString();
    }
}
