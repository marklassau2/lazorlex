package com.lazornet.grammar.lex.lexercompiler;

import java.util.List;

public final class LexerRule {
    private final String tokenName;
    private final int index;
    private final List<String> states;
    private final RegexDef regexDef;
    private final String exactText;
    private final String nextState;

    LexerRule(String tokenName, int index, List<String> states, RegexDef regexDef, String exactText, String nextState) {
        this.tokenName = tokenName;
        this.index = index;
        this.states = states;
        this.regexDef = regexDef;
        this.exactText = exactText;
        this.nextState = nextState;
    }

    public String getTokenName() {
        return tokenName;
    }

    public int getIndex() {
        return index;
    }

    public List<String> getStates() {
        return states;
    }

    public boolean isExactText() {
        return exactText != null;
    }

    public boolean isRegex() {
        return exactText == null;
    }

    public RegexDef getRegexDef() {
        return regexDef;
    }

    public String getExactText() {
        return exactText;
    }

    public String getNextState() {
        return nextState;
    }
}
