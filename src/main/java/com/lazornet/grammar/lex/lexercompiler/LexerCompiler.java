package com.lazornet.grammar.lex.lexercompiler;

import com.lazornet.grammar.lazorlex.lex.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class LexerCompiler {
    public static LexerDefinition compile(Statements statements) {
        final List<String> stateNames = extractStateNames(statements);

        List<LexerRule> rules = extractRules(statements, stateNames);
        return new LexerDefinition(rules, stateNames);
    }

    private static List<LexerRule> extractRules(Statements statements, List<String> allStates) {
        final List<LexerRule> rules = new LinkedList<>();
        int index = 0;
        for (Statement statement : statements.getStatements()) {
            if (statement.hasRuleDefinition()) {
                // global rule
                rules.add(extractRule(statement.getRuleDefinition(), index++, allStates));
            } else {
                final List<String> singleState = Collections.singletonList(statement.getStateDefinition().getStateName().getText());
                for (RuleDefinition ruleDefinition : statement.getStateDefinition().getRuleDefinitions()) {
                    rules.add(extractRule(ruleDefinition, index++, singleState));
                }
            }
        }
        return rules;
    }

    private static LexerRule extractRule(RuleDefinition ruleDefinition, int index, List<String> states) {
        final String tokenName = ruleDefinition.getTokenName().getText();

        final RegexDef regexDef;
        final String exactText;
        if (ruleDefinition.getTokenLexer().hasExactTextLexer()) {
            regexDef = null;
            exactText = ruleDefinition.getTokenLexer().getExactTextLexer().getText().getText();
        } else {
            regexDef = compileRegex(ruleDefinition.getTokenLexer().getRegexLexer().getRegex());
            exactText = null;
        }

        final String nextState = ruleDefinition.hasStateName() ? ruleDefinition.getStateName().getText() : null;
        return new LexerRule(tokenName, index, states, regexDef, exactText, nextState);
    }

    private static RegexDef compileRegex(Regex regex) {
        final List<RegexDefChunk> chunks = new LinkedList<>();

        for (RegexChunk regexChunk : regex.getRegexChunks()) {
            chunks.add(compileRegexChunk(regexChunk));
        }

        return new RegexDef(chunks);
    }

    private static RegexDefChunk compileRegexChunk(RegexChunk regexChunk) {
        final RegexChar regexChar = regexChunk.getRegexAtom().fold(
                dot -> RegexChar.ANY,
                ch -> RegexChar.single(ch.getText().charAt(0)),
                regexAlt -> RegexChar.alternatives(regexAlt.getRegexAltChunks().stream()
                            .map(LexerCompiler::compileAlt)
                            .collect(Collectors.toList())),
                LexerCompiler::compileEscapeChar
        );
        // but is it negated?
        final boolean negated = regexChunk.hasTilde();

        final RegexDefChunk.Quantifier quantifier;
        if (regexChunk.hasRegexQuantifier()) {
            RegexQuantifier parsedQuantifier = regexChunk.getRegexQuantifier();
            if (parsedQuantifier.getPlus() != null) {
                quantifier = RegexDefChunk.Quantifier.ONE_OR_MORE;
            } else if (parsedQuantifier.getQuestionMark() != null) {
                quantifier = RegexDefChunk.Quantifier.MAYBE_ONE;
            } else if (parsedQuantifier.getStar() != null) {
                quantifier = RegexDefChunk.Quantifier.ZERO_OR_MORE;
            } else {
                throw new IllegalStateException();
            }
        } else {
            quantifier = RegexDefChunk.Quantifier.EXACTLY_ONE;
        }
        return new RegexDefChunk(regexChar.negated(negated), quantifier);
    }

    private static RegexChar compileEscapeChar(RegexEscape regexEscape) {
        switch (regexEscape.type()) {
            case ESC_BACKSLASH:
                return RegexChar.single('\\');
            case ESC_PLUS:
                return RegexChar.single('+');
            case ESC_STAR:
                return RegexChar.single('*');
            case ESC_QUEST_MARK:
                return RegexChar.single('?');
            case ESC_TILDE:
                return RegexChar.single('~');
            default:
                throw new IllegalArgumentException("Unknown escape token: " + regexEscape.type());
        }
    }

    private static RegexAlternative compileAlt(RegexAltChunk regexAltChunk) {
        if (regexAltChunk.getChar() != null) {
            return RegexAlternative.single(regexAltChunk.getChar().getText().charAt(0));
        } else if (regexAltChunk.getEscBackslash() != null) {
            return RegexAlternative.single('\\');
        } else if (regexAltChunk.getEscCr() != null) {
            return RegexAlternative.single('\r');
        } else if (regexAltChunk.getEscLf() != null) {
            return RegexAlternative.single('\n');
        } else if (regexAltChunk.getEscTab() != null) {
            return RegexAlternative.single('\t');
        } else if (regexAltChunk.getRegexAltRange() != null) {
            final char start = regexAltChunk.getRegexAltRange().getChars().get(0).getText().charAt(0);
            final char end = regexAltChunk.getRegexAltRange().getChars().get(1).getText().charAt(0);
            return RegexAlternative.range(start, end);
        } else {
            throw new IllegalStateException();
        }
    }

    private static List<String> extractStateNames(Statements statements) {
        final List<String> stateNames = new LinkedList<>();
        for (Statement statement : statements.getStatements()) {
            if (statement.getStateDefinition() != null) {
                final String stateName = statement.getStateDefinition().getStateName().getText();
                if (!stateNames.contains(stateName)) {
                    stateNames.add(stateName);
                }
            }
        }
        if (stateNames.isEmpty()) {
            // all rules are global: create a single default state
            return Collections.singletonList("default");
        } else {
            return stateNames;
        }
    }
}
