package com.lazornet.grammar.lex.lexercompiler;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AlternativeRegexChars implements RegexChar {
    private final List<RegexAlternative> alternatives;

    AlternativeRegexChars(List<RegexAlternative> alternatives) {
        this.alternatives = alternatives;
    }

    @Override
    public void generateMatchCode(@NotNull StringBuilder sb) {
        // ch >= 'a' &&  ch <= 'z' || ch == '_'

        boolean first = true;
        for (RegexAlternative alternative : alternatives) {
            if (first) {
                first = false;
            } else {
                sb.append(" || ");
            }
            alternative.generateMatchCode(sb);
        }
    }

    @Override
    public RegexChar negated() {
        return new Negated(alternatives);
    }

    @Override
    public String decompile() {
        return decompile(alternatives);
    }

    private static String decompile(List<RegexAlternative> alternatives) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (RegexAlternative alternative : alternatives) {
            alternative.decompile(sb);
        }
        sb.append(']');
        return sb.toString();
    }

    private static class Negated implements RegexChar {
        private final List<RegexAlternative> alternatives;

        Negated(List<RegexAlternative> alternatives) {
            this.alternatives = alternatives;
        }

        @Override
        public RegexChar negated() {
            return new AlternativeRegexChars(alternatives);
        }

        @Override
        public String decompile() {
            return "~" + AlternativeRegexChars.decompile(alternatives);
        }

        @Override
        public void generateMatchCode(@NotNull StringBuilder sb) {
            // !(ch >= 'a' &&  ch <= 'z' || ch == '_' || ch == (char) 0)

            sb.append("!(");
            for (RegexAlternative alternative : alternatives) {
                alternative.generateMatchCode(sb);
                sb.append(" || ");
            }
            // ensure we break out of the loop on EOF
            sb.append("ch == (char) 0)");
        }
    }
}
