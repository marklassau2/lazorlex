package com.lazornet.grammar.lex.lexercompiler;

import static com.lazornet.grammar.lex.util.CharTools.escape;

interface RegexAlternative {
    static RegexAlternative single(char ch) {
        return new RegexAlternative() {
            @Override
            public void generateMatchCode(StringBuilder sb) {
                sb.append("ch == '").append(escape(ch)).append('\'');
            }

            @Override
            public void decompile(StringBuilder sb) {
                sb.append(ch);
            }
        };
    }

    static RegexAlternative range(char start, char end) {
        return new RegexAlternative() {
            @Override
            public void generateMatchCode(StringBuilder sb) {
                sb.append("ch >= '").append(escape(start)).append('\'');
                sb.append(" && ");
                sb.append("ch <= '").append(escape(end)).append('\'');
            }

            @Override
            public void decompile(StringBuilder sb) {
                sb.append(start).append('-').append(end);
            }
        };
    }

    void generateMatchCode(StringBuilder sb);

    void decompile(StringBuilder sb);
}
