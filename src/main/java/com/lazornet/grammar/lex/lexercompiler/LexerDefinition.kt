package com.lazornet.grammar.lex.lexercompiler

import java.lang.IllegalStateException

class LexerDefinition(val rules: List<LexerRule>, stateNames: List<String>) {
    val tokenTypes: List<String>
    val states: List<LexerState>

    init {
        val stateToRules = mutableMapOf<String, MutableList<LexerRule>>()

        for (rule in rules) {

            val ruleStates = if (rule.states == null || rule.states.isEmpty()) {
                // This is a rule for all states
                stateNames
            } else {
                rule.states
            }

            for (state in ruleStates) {
                val stateRules = stateToRules.getOrPut(state, { mutableListOf() })
                stateRules.add(rule)
            }
        }

        val _states = mutableListOf<LexerState>()
        for (stateName in stateNames) {
            val rules = stateToRules[stateName] ?: throw IllegalStateException("No rules found for state $stateName")
            val state = LexerState(stateName, rules)
            _states.add(state)
        }
        states = _states.toList()

        val tokenNameSet = mutableSetOf<String>()
        rules.forEach { r-> tokenNameSet.add(r.tokenName) }
        tokenTypes = tokenNameSet.toList().sorted()
    }

    fun stateIndex(stateName: String?): Int {
        if (stateName == null) {
            return -1
        } else {
            for ((idx, state) in states.withIndex()) {
                if (state.name == stateName) return idx
            }
        }
        throw IllegalStateException("Unable to find state named '$stateName'")
    }

}
