package com.lazornet.grammar.lex.lexercompiler;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.lazornet.grammar.lex.util.CharTools.escape;

public interface RegexChar {
    RegexChar ANY = new RegexChar() {
        @Override
        public void generateMatchCode(@NotNull StringBuilder sb) {
            sb.append("ch > (char) 0");
        }

        @Override
        public RegexChar negated() {
            return new RegexChar() {
                @Override
                public void generateMatchCode(@NotNull StringBuilder sb) {
                    sb.append("false");
                }

                @Override
                public RegexChar negated() {
                    throw new UnsupportedOperationException("Double negation not supported in regex");
                }

                @Override
                public String decompile() {
                    return "~.";
                }
            };
        }

        @Override
        public String decompile() {
            return ".";
        }
    };

    static RegexChar alternatives(List<RegexAlternative> alternatives) {
        return new AlternativeRegexChars(alternatives);
    }

    static RegexChar single(char ch) {
        return new RegexChar() {
            @Override
            public void generateMatchCode(@NotNull StringBuilder sb) {
                // ch == 'a'
                sb.append("ch == '").append(escape(ch)).append('\'');
            }

            @Override
            public RegexChar negated() {
                return new RegexChar() {
                    @Override
                    public void generateMatchCode(@NotNull StringBuilder sb) {
                        sb.append("ch != '").append(escape(ch)).append("' && ch != (char) 0");
                    }

                    @Override
                    public RegexChar negated() {
                        throw new UnsupportedOperationException("Double negation not supported in regex");
                    }

                    @Override
                    public String decompile() {
                        return "~" + ch;
                    }
                };
            }

            @Override
            public String decompile() {
                return "" + ch;
            }
        };
    }

    void generateMatchCode(@NotNull StringBuilder sb);

    default RegexChar negated(boolean negated) {
        return negated ? this.negated() : this;
    }

    RegexChar negated();

    String decompile();
}
