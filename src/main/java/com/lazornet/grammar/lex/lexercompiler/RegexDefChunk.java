package com.lazornet.grammar.lex.lexercompiler;

import org.jetbrains.annotations.NotNull;

public class RegexDefChunk {
    public enum Quantifier { EXACTLY_ONE, MAYBE_ONE, ZERO_OR_MORE, ONE_OR_MORE}

    private final RegexChar regexChar;
    private final Quantifier quantifier;

    RegexDefChunk(RegexChar regexChar, Quantifier quantifier) {

        this.regexChar = regexChar;
        this.quantifier = quantifier;
    }

    public RegexChar getRegexChar() {
        return regexChar;
    }

    @NotNull
    public Quantifier getQuantifier() {
        return quantifier;
    }
}
