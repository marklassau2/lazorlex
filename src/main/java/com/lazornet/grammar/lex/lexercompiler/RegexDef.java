package com.lazornet.grammar.lex.lexercompiler;

import java.util.List;

public class RegexDef {
    private final List<RegexDefChunk> chunks;

    RegexDef(List<RegexDefChunk> chunks) {
        this.chunks = chunks;
    }

    public List<RegexDefChunk> getChunks() {
        return chunks;
    }

    public String decompile() {
        StringBuilder sb = new StringBuilder();
        for (RegexDefChunk chunk : chunks) {
            sb.append(chunk.getRegexChar().decompile());
            switch (chunk.getQuantifier()) {
                case MAYBE_ONE:
                    sb.append('?');
                    break;
                case ZERO_OR_MORE:
                    sb.append('*');
                    break;
                case ONE_OR_MORE:
                    sb.append('+');
                    break;
            }
        }

        return sb.toString();
    }
}
