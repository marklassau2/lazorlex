package com.lazornet.grammar.lex.lexercompiler;

import java.util.List;

public class LexerState {
    private final String name;
    private final List<LexerRule> rules;

    LexerState(String name, List<LexerRule> rules) {
        this.name = name;
        this.rules = rules;
    }

    public List<LexerRule> getRules() {
        return rules;
    }

    public String getName() {
        return name;
    }
}
