package com.lazornet.grammar.lex;

import com.lazornet.grammar.lex.lexergenerator.LexerGenerator;
import com.lazornet.grammar.lex.parsergenerator.ParserGenerator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.*;

/**
 * Generates java source code for Lexers
 */
@Mojo( name = "java", defaultPhase = LifecyclePhase.GENERATE_SOURCES, threadSafe = true )
public class LazorlexMojo extends AbstractMojo
{
    /**
     * The output directory for the generated source code
     */
    @Parameter( defaultValue = "${project.build.directory}/generated-sources/lazorlex", required = true )
    private File outputDirectory;

    /**
     * The input directory for the grammar files
     */
    @Parameter( defaultValue = "src/main/resources/lazorlex", required = true )
    private File inputDirectory;

    /**
     * The Maven project instance for the executing project.
     */
    @Parameter( defaultValue = "${project}", readonly = true, required = true )
    private MavenProject project;

    /**
     * The package for the generated source code
     */
    @Parameter( property = "package", required = true )
    private String outputPackage;

    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info( "Inspecting input directory:  " + inputDirectory.getAbsolutePath());
        getLog().info( "Generating classes in package: " + outputPackage );
        getLog().info( "Writing to output directory: " + outputDirectory.getAbsolutePath());

        inspectInputFiles(inputDirectory, outputPackage);

        project.addCompileSourceRoot(outputDirectory.getAbsolutePath());
    }

    private void inspectInputFiles(File inputDirectory, String outputPackage) throws MojoExecutionException {
        File[] files = inputDirectory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    // inspect sub-directory recursively
                    inspectInputFiles(file, outputPackage + "." + file.getName());
                }
                if (file.getName().endsWith(".lex")) {
                    getLog().info( "Generating Lexer from " + file.getName());
                    new LexerGenerator(outputDirectory, outputPackage, file, getLog()).generateLexer();
                }
                if (file.getName().endsWith(".grm")) {
                    getLog().info( "Generating Parser from " + file.getName());
                    new ParserGenerator(outputDirectory, outputPackage, file, getLog()).generateParser();
                }
            }
        } else {
            // failed to list files - get more details
            if (inputDirectory.exists()) {
                if (inputDirectory.isDirectory()) {
                    // WAT?
                    throw new MojoExecutionException("Failed to read files in " + inputDirectory.getAbsolutePath());
                } else {
                    throw new MojoExecutionException("Not a directory: " + inputDirectory.getAbsolutePath());
                }
            } else {
                throw new MojoExecutionException("No such directory: " + inputDirectory.getAbsolutePath());
            }
        }
    }
}