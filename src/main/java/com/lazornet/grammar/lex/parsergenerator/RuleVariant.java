package com.lazornet.grammar.lex.parsergenerator;

import java.util.List;

public final class RuleVariant {
    private final List<QuantifiedRule> quantifiedRules;

    public RuleVariant(List<QuantifiedRule> quantifiedRules) {
        this.quantifiedRules = quantifiedRules;
    }

    public List<QuantifiedRule> getQuantifiedRules() {
        return quantifiedRules;
    }

    @Override
    public String toString() {
        return quantifiedRules.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RuleVariant that = (RuleVariant) o;

        return quantifiedRules.equals(that.quantifiedRules);
    }

    @Override
    public int hashCode() {
        return quantifiedRules.hashCode();
    }
}
