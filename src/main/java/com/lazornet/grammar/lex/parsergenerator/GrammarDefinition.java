package com.lazornet.grammar.lex.parsergenerator;

import java.util.List;

public class GrammarDefinition {
    private final List<NamedProductionRule> namedRules;
    private final List<String> tokenNames;

    public GrammarDefinition(List<NamedProductionRule> namedRules, List<String> tokenNames) {
        this.namedRules = namedRules;
        this.tokenNames = tokenNames;
    }

    public List<NamedProductionRule> getNamedRules() {
        return namedRules;
    }

    public List<String> getTokenNames() {
        return tokenNames;
    }
}
