package com.lazornet.grammar.lex.parsergenerator;

import com.lazornet.grammar.lazorlex.grammar.CompoundRule;
import com.lazornet.grammar.lazorlex.grammar.Quantifier;
import com.lazornet.grammar.lazorlex.grammar.RuleSequence;
import com.lazornet.grammar.lazorlex.grammar.SingleRule;

class CardinalityAnalyser {

    static CardinalityMap analyseCardinality(CompoundRule compoundRule) {
        CardinalityMap cardinalityMap = null;
        for (RuleSequence ruleSequence : compoundRule.getRuleSequences()) {
            CardinalityMap cardinalityMap2 = analyseCardinality(ruleSequence);
            if (cardinalityMap == null) {
                cardinalityMap = cardinalityMap2;
            } else {
                cardinalityMap = cardinalityMap.or(cardinalityMap2);
            }
        }
        return cardinalityMap;
    }

    private static CardinalityMap analyseCardinality(RuleSequence ruleSequence) {
        CardinalityMap cardinalityMap = null;
        for (com.lazornet.grammar.lazorlex.grammar.QuantifiedRule quantifiedRule : ruleSequence.getQuantifiedRules()) {
            CardinalityMap cardinalityMap2 = analyseCardinality(quantifiedRule);
            if (cardinalityMap == null) {
                cardinalityMap = cardinalityMap2;
            } else {
                cardinalityMap = cardinalityMap.plus(cardinalityMap2);
            }
        }
        return cardinalityMap;
    }

    private static CardinalityMap analyseCardinality(com.lazornet.grammar.lazorlex.grammar.QuantifiedRule quantifiedRule) {
        CardinalityMap cardinalityMap = analyseCardinality(quantifiedRule.getSingleRule());
        Quantifier quantifier = quantifiedRule.getQuantifier();
        if (quantifier == null) {
            return cardinalityMap;
        }
        if (quantifier.getQuestionMark() != null) {
            return cardinalityMap.maybeOne();
        } else {
            return cardinalityMap.many();
        }
    }

    private static CardinalityMap analyseCardinality(SingleRule singleRule) {
        return singleRule.fold(
                tn -> CardinalityMap.ofOne(tn.getText()),
                rn -> CardinalityMap.ofOne(rn.getText()),
                tn -> analyseCardinality(tn.getCompoundRule())
        );
    }
}
