package com.lazornet.grammar.lex.parsergenerator

import java.io.PrintWriter

class TokenNodeClassGenerator(private val packageName: String, private val tokenName: String, private val className: String) {
    fun generate(out: PrintWriter) {
        out.println("""
package $packageName;

/**
 * This node represents the $tokenName token.
 */
public class $className extends AstNode.TokenNode {

    public $className(String text) {
        super(text);
    }
}
""".trimIndent())
    }

}