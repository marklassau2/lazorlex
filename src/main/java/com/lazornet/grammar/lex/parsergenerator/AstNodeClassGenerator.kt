package com.lazornet.grammar.lex.parsergenerator

import java.io.PrintWriter

class AstNodeClassGenerator(private val packageName: String) {
    fun generate(out: PrintWriter) {
        out.println("""
package $packageName;

import java.util.List;

public interface AstNode {

    class ParentNode implements AstNode {
        private final List<AstNode> childNodes;
        
        ParentNode(List<AstNode> childNodes) {
            this.childNodes = childNodes;
        }
        
        public List<AstNode> getChildNodes() {
            return childNodes;
        }
    }

    class TokenNode implements AstNode {
        private final String text;

        TokenNode(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }
}
""".trimIndent())
    }

}