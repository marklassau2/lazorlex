package com.lazornet.grammar.lex.parsergenerator;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class QuantifiedRule {
    public enum Quantifier {ONE(""), MAYBE_ONE("?"), ONE_OR_MORE("+"), ZERO_OR_MORE("*");
        public final String suffix;

        Quantifier(String suffix) {
            this.suffix = suffix;
        }
    }

    private final String singleRuleName;
    private final List<RuleVariant> nestedVariants;
    private final Quantifier quantifier;

    public QuantifiedRule(String singleRuleName, List<RuleVariant> nestedVariants, Quantifier quantifier) {
        this.singleRuleName = singleRuleName;
        this.nestedVariants = nestedVariants;
        this.quantifier = quantifier;
    }

    public boolean isNestedRule() {
        return singleRuleName == null;
    }

    public String getSingleRuleName() {
        return singleRuleName;
    }

    public List<RuleVariant> getNestedVariants() {
        return nestedVariants;
    }

    @NotNull
    public Quantifier getQuantifier() {
        return quantifier;
    }

    @Override
    public String toString() {
        if (singleRuleName == null) {
            return "NestedRule{" +
                    "nestedVariants=" + nestedVariants +
                    '}' + quantifier.suffix;
        } else {
            return singleRuleName + quantifier.suffix;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuantifiedRule that = (QuantifiedRule) o;

        if (singleRuleName != null ? !singleRuleName.equals(that.singleRuleName) : that.singleRuleName != null)
            return false;
        if (nestedVariants != null ? !nestedVariants.equals(that.nestedVariants) : that.nestedVariants != null)
            return false;
        return quantifier == that.quantifier;
    }

    @Override
    public int hashCode() {
        int result = singleRuleName != null ? singleRuleName.hashCode() : 0;
        result = 31 * result + (nestedVariants != null ? nestedVariants.hashCode() : 0);
        result = 31 * result + quantifier.hashCode();
        return result;
    }
}
