package com.lazornet.grammar.lex.parsergenerator;

import com.lazornet.grammar.lazorlex.grammar.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GrammarCompiler {

    public static GrammarDefinition compileGrammarDefinition(ProductionRules productionRules) {
        List<NamedProductionRule> namedProductionRules = new ArrayList<>(productionRules.getProductionRules().size());
        Set<String> tokenNameSet = new HashSet<>();

        for (ProductionRule productionRule : productionRules.getProductionRules()) {
            extractTokenNames(productionRule.getCompoundRule(), tokenNameSet);
            namedProductionRules.add(compile(productionRule));
        }

        List<String> tokenNameList = new ArrayList<>(tokenNameSet.size());
        tokenNameList.addAll(tokenNameSet);
        return new GrammarDefinition(namedProductionRules, tokenNameList);
    }

    private static NamedProductionRule compile(ProductionRule productionRule) {
        String name = productionRule.getRuleName().getText();
        // extract collection of named AST nodes and the cardinality of each
        CardinalityMap cardinalityMap = CardinalityAnalyser.analyseCardinality(productionRule.getCompoundRule());

        List<RuleVariant> variants = productionRule.getCompoundRule().getRuleSequences().stream()
                .map(GrammarCompiler::compileRuleSequence)
                .collect(Collectors.toList());
        return new NamedProductionRule(name, variants, cardinalityMap);
    }

    private static RuleVariant compileRuleSequence(RuleSequence ruleSequence) {
        List<QuantifiedRule> quantifiedRules = ruleSequence.getQuantifiedRules().stream()
                .map(GrammarCompiler::parseQuantifiedRule)
                .collect(Collectors.toList());
        return new RuleVariant(quantifiedRules);
    }

    private static QuantifiedRule parseQuantifiedRule(com.lazornet.grammar.lazorlex.grammar.QuantifiedRule quantifiedRule) {
        final String singleRuleName;
        final List<RuleVariant> nestedVariants;
        if (quantifiedRule.getSingleRule().hasRuleName()) {
            singleRuleName = quantifiedRule.getSingleRule().getRuleName().getText();
            nestedVariants = null;
        } else if (quantifiedRule.getSingleRule().hasTokenName()) {
            singleRuleName = quantifiedRule.getSingleRule().getTokenName().getText();
            nestedVariants = null;
        } else {
            singleRuleName = null;
            nestedVariants = quantifiedRule.getSingleRule().getNestedRule().getCompoundRule().getRuleSequences().stream()
                    .map(GrammarCompiler::compileRuleSequence)
                    .collect(Collectors.toList());
        }

        final QuantifiedRule.Quantifier quantifier;
        if (quantifiedRule.getQuantifier() == null) {
            quantifier = QuantifiedRule.Quantifier.ONE;
        } else if (quantifiedRule.getQuantifier().getPlus() != null) {
            quantifier = QuantifiedRule.Quantifier.ONE_OR_MORE;
        } else if (quantifiedRule.getQuantifier().getStar() != null) {
            quantifier = QuantifiedRule.Quantifier.ZERO_OR_MORE;
        } else if (quantifiedRule.getQuantifier().getQuestionMark() != null) {
            quantifier = QuantifiedRule.Quantifier.MAYBE_ONE;
        } else {
            throw new IllegalStateException();
        }

        return new QuantifiedRule(singleRuleName, nestedVariants, quantifier);
    }

    private static void extractTokenNames(CompoundRule compoundRule, Set<String> tokenNameSet) {
        for (RuleSequence ruleSequence : compoundRule.getRuleSequences()) {
            for (com.lazornet.grammar.lazorlex.grammar.QuantifiedRule quantifiedRule : ruleSequence.getQuantifiedRules()) {
                extractTokenNames(quantifiedRule.getSingleRule(), tokenNameSet);
            }
        }
    }

    private static void extractTokenNames(SingleRule singleRule, Set<String> tokenNameSet) {
        // If the rule is a simple token name - add it to the set
        // If the rule is a simple rule name - nothing to do here (the named rule will inspect its own tokens later)
        // If the rule is a nested rule - recursively find the tokens
        if (singleRule.hasTokenName()) {
            tokenNameSet.add(singleRule.getTokenName().getText());
        } else if (singleRule.hasNestedRule()) {
            extractTokenNames(singleRule.getNestedRule().getCompoundRule(), tokenNameSet);
        }
    }
}
