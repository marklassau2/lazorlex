package com.lazornet.grammar.lex.parsergenerator

import java.io.PrintWriter

class TokenStreamClassGenerator(private val packageName: String, private val lexerName: String) {
    fun generate(out: PrintWriter) {
        out.println("""
package $packageName;

import java.util.List;

public class ${lexerName}TokenStream {
    private final List<${lexerName}Token> tokens;
    private int index = 0;

    public ${lexerName}TokenStream(List<${lexerName}Token> tokens) {
        this.tokens = tokens;
    }

    public boolean isEof() {
        return index >= tokens.size();
    }

    public Marker mark() {
        return new Marker(index);
    }

    public void rollback(Marker marker) {
        index = marker.index;
    }

    public ${lexerName}Token.Type tokenType() {
        if (isEof()) return null;
        return tokens.get(index).getTokenType();
    }

    public ${lexerName}Token next() {
        if (isEof()) return null;
        return tokens.get(index++);
    }

    public int getIndex() {
        return index;
    }
    
    public int getCharIndex(int tokenIndex) {
        if (tokenIndex >= tokens.size()) {
            // EOF
            ${lexerName}Token finalToken = tokens.get(tokens.size() - 1);
            return finalToken.getTokenStart() + finalToken.getText().length();
        } else {
            return tokens.get(tokenIndex).getTokenStart();
        }
    }
    
    static class Marker {
        private final int index;

        private Marker(int index) {
            this.index = index;
        }
    }
}

""".trimIndent())
    }

}