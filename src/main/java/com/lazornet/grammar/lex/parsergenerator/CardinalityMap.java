package com.lazornet.grammar.lex.parsergenerator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class CardinalityMap {
    private final Map<String, Cardinality> map;

    private CardinalityMap(Map<String, Cardinality> map) {
        this.map = map;
    }

    static CardinalityMap ofOne(String nodeName) {
        return new CardinalityMap(Collections.singletonMap(nodeName, Cardinality.ONE));
    }

     CardinalityMap or(CardinalityMap other) {
        Map<String, Cardinality> newMap = new HashMap<>();
        for (Map.Entry<String, Cardinality> entry : map.entrySet()) {
            final Cardinality thisCardinality = entry.getValue();
            Cardinality otherCardinality = other.map.get(entry.getKey());
            newMap.put(entry.getKey(), thisCardinality.or(otherCardinality));
        }
        // now check for nodes in the other map not in this map
        for (Map.Entry<String, Cardinality> entry : other.map.entrySet()) {
            if (!map.containsKey(entry.getKey())) {
                Cardinality cardinality = entry.getValue() == Cardinality.MANY ? Cardinality.MANY : Cardinality.MAYBE_ONE;
                newMap.put(entry.getKey(), cardinality);
            }
        }
        return new CardinalityMap(newMap);
    }

    /**
     * Return the cardinality of this rule followed by the other rule in a sequence.
     * @param other the other CardinalityMap
     * @return the cardinality of this rule followed by the other rule in a sequence.
     */
    public CardinalityMap plus(CardinalityMap other) {
        Map<String, Cardinality> newMap = new HashMap<>();
        for (Map.Entry<String, Cardinality> entry : map.entrySet()) {
            final Cardinality thisCardinality = entry.getValue();
            Cardinality otherCardinality = other.map.get(entry.getKey());
            newMap.put(entry.getKey(), thisCardinality.plus(otherCardinality));
        }
        // now check for nodes in the other map not in this map
        for (Map.Entry<String, Cardinality> entry : other.map.entrySet()) {
            if (!map.containsKey(entry.getKey())) {
                newMap.put(entry.getKey(), entry.getValue());
            }
        }
        return new CardinalityMap(newMap);
    }

    public Set<Map.Entry<String, Cardinality>> entries() {
        return map.entrySet();
    }

    /**
     * Applies a maybe operator to this CardinalityMap.
     * eg (FOO | FOO BAR* OOF)?
     */
    CardinalityMap maybeOne() {
        Map<String, Cardinality> newMap = new HashMap<>();
        for (Map.Entry<String, Cardinality> entry : map.entrySet()) {
            final Cardinality cardinality = entry.getValue() == Cardinality.ONE ? Cardinality.MAYBE_ONE : entry.getValue();
            newMap.put(entry.getKey(), cardinality);
        }
        return new CardinalityMap(newMap);
    }

    /**
     * Applies a many operator to this CardinalityMap.
     * <p>
     * eg <pre>
     *    (FOO | FOO BAR* OOF)+
     *    (FOO | FOO BAR* OOF)*
     * </pre>
     */
    CardinalityMap many() {
        Map<String, Cardinality> newMap = new HashMap<>();
        for (String nodeName : map.keySet()) {
            newMap.put(nodeName, Cardinality.MANY);
        }
        return new CardinalityMap(newMap);
    }

    Cardinality get(String key) {
        return map.get(key);
    }

    @Override
    public String toString() {
        return map.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardinalityMap that = (CardinalityMap) o;

        return map.equals(that.map);
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }
}
