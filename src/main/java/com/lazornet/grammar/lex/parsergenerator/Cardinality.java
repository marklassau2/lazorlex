package com.lazornet.grammar.lex.parsergenerator;

public enum Cardinality {
    ONE {
        @Override
        Cardinality plus(Cardinality other) {
            if (other == null) {
                return ONE;
            } else {
                return MANY;
            }
        }

        @Override
        Cardinality or(Cardinality other) {
            if (other == ONE) {
                return ONE;
            } else if (other == MANY){
                return MANY;
            } else {
                return MAYBE_ONE;
            }
        }
    },
    MAYBE_ONE {
        @Override
        Cardinality plus(Cardinality other) {
            if (other == null) {
                return MAYBE_ONE;
            } else {
                return MANY;
            }
        }

        @Override
        Cardinality or(Cardinality other) {
            if (other == MANY){
                return MANY;
            } else {
                return MAYBE_ONE;
            }
        }
    },
    MANY {
        @Override
        Cardinality plus(Cardinality other) {
            return MANY;
        }

        @Override
        Cardinality or(Cardinality other) {
            return MANY;
        }
    };

    abstract Cardinality plus(Cardinality other);

    abstract Cardinality or(Cardinality other);
}
