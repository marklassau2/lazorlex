package com.lazornet.grammar.lex.parsergenerator

import com.lazornet.grammar.lex.util.ClassNameUtil
import java.io.PrintWriter

class ParserClassGenerator(private val packageName: String, private val lexerName: String, private val grammarDefinition: GrammarDefinition) {
    fun generate(out: PrintWriter) {
        val resultClassName = ClassNameUtil.classNameFor(grammarDefinition.namedRules[0].name)

        out.println("""
package $packageName;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class ${lexerName}Parser {
    private final ${lexerName}TokenStream tokenStream;
    private final CharSequence text;
    private final FailRegister failRegister = new FailRegister();

    /**
     * Parse the given list of tokens.
     * 
     * @param tokens the tokens emitted by the Lexer
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List, CharSequence) 
     */
    public static $resultClassName parse(List<${lexerName}Token> tokens) throws ${lexerName}ParseException {
        return new ${lexerName}Parser(tokens, null).parse();
    }

    /**
     * Parse the given list of tokens.
     * <p>
     * This version of <pre>parse()</pre> accepts the original text to be parsed as a param. This is purely to be used
     * to in error handling to calculate the error position as a line number + column number. If the original text is 
     * not provided then the character position in the stream is displayed instead.
     *
     * @param tokens the tokens emitted by the Lexer
     * @param text the original text that was tokenised - this is only used for error messaging              
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List)
     */
    public static $resultClassName parse(List<${lexerName}Token> tokens, CharSequence text) throws ${lexerName}ParseException {
        return new ${lexerName}Parser(tokens, text).parse();
    }

    private $resultClassName parse() throws ${lexerName}ParseException {
        $resultClassName result = parse$resultClassName();
        if (result != null && tokenStream.isEof()) {
            return result;
        } else {
            throw new ${lexerName}ParseException(tokenStream.getCharIndex(failRegister.depth), failRegister.expectedTokens, text);
        }
    }

    private ${lexerName}Parser(List<${lexerName}Token> tokens, CharSequence text) {
        this.tokenStream = new ${lexerName}TokenStream(tokens);
        this.text = text;
    }
""".trimIndent())
        generateParseMethods(out)

        out.println("""

    private static final class FailRegister {
        private int depth = -1;
        private final HashSet<${lexerName}Token.Type> expectedTokens = new HashSet<>();

        public void registerFail(int index, ${lexerName}Token.Type expectedToken) {
            if (index == depth) {
                expectedTokens.add(expectedToken);
            } else if (index > depth) {
                depth = index;
                expectedTokens.clear();
                expectedTokens.add(expectedToken);
            }
        }
    }
}    
""".trimIndent())
    }

    private fun generateParseMethods(out: PrintWriter) {
        grammarDefinition.namedRules.forEach {
            generateParseMethod(out, it)
        }
        grammarDefinition.tokenNames.forEach {
            generateTokenParseMethods(out, it)
        }
    }

    private fun generateTokenParseMethods(out: PrintWriter, tokenName: String) {
        //    private Star parseStar() {
        //        if (tokenStream.tokenType() == GrammarToken.Type.STAR) {
        //            return new Star(tokenStream.next());
        //        } else {
        //            return null;
        //        }
        //    }
        val className = ClassNameUtil.classNameFor(tokenName)

        out.println("    private $className parse$className() {")
        out.println("        if (tokenStream.tokenType() == ${lexerName}Token.Type.$tokenName) {")
        out.println("            return new $className(tokenStream.next().getText());")
        out.println("        } else {")
        out.println("            failRegister.registerFail(tokenStream.getIndex(), ${lexerName}Token.Type.$tokenName);")
        out.println("            return null;")
        out.println("        }")
        out.println("    }")
    }

    private fun generateParseMethod(out: PrintWriter, namedRule: NamedProductionRule) {
        val className = ClassNameUtil.classNameFor(namedRule.name)
        generateParseMethod(out, "parse$className", className, namedRule.variants)
    }

    private fun generateNestedParseMethod(out: PrintWriter, nestedRule: NestedRuleInfo) {
        generateParseMethod(out, nestedRule.methodName, "List<AstNode>", nestedRule.variants)
    }

    private fun generateParseMethod(out: PrintWriter, methodName: String, returnType: String, variants: List<RuleVariant>) {
        out.println()
        out.println("    private $returnType $methodName() {")
        if (variants.size > 1) {
            out.println("        $returnType result;")
        }
        val lastVariantIdx = variants.size - 1

        variants.forEachIndexed { v_idx, _ ->
            if (v_idx < lastVariantIdx) {
                out.println("        result = ${methodName}_v$v_idx();")
                out.println("        if (result != null) return result;")
            } else {
                out.println("        return ${methodName}_v$v_idx();")
            }
        }

        out.println("    }")


        variants.forEachIndexed { v_idx, variant ->
            val nestedRules = mutableListOf<NestedRuleInfo>()

            out.println()
            out.println("    private $returnType ${methodName}_v$v_idx() {")
            out.println("        List<AstNode> childNodes = new LinkedList<>();")
            out.println("        ${lexerName}TokenStream.Marker marker = tokenStream.mark();")
            out.println()
            variant.quantifiedRules.forEachIndexed { r_idx, qr ->

                val addMethod: String
                val parseMethod: String
                if (qr.isNestedRule) {
                    addMethod = "addAll"
                    parseMethod = "${methodName}_v${v_idx}_r$r_idx"
                    out.println("        List<AstNode> r$r_idx = $parseMethod();")
                    nestedRules.add(NestedRuleInfo(parseMethod, qr.nestedVariants))
                } else {
                    addMethod = "add"
                    parseMethod = "parse${ClassNameUtil.classNameFor(qr.singleRuleName)}"
                    out.println("        AstNode r$r_idx = $parseMethod();")
                }

                //        AstNode r0 = parseStar();
                //        if (r0 == null) {
                //            tokenStream.rollback(marker);
                //            return null;
                //        } else {
                //            childNodes.add(r0);
                //        }
                //
                //        AstNode r1 = parseStar();
                //        if (r1 != null) {
                //            childNodes.add(r1);
                //        }
                //
                //        AstNode r2 = parseStar();
                //        if (r2 == null) {
                //            tokenStream.rollback(marker);
                //            return null;
                //        }
                //        while (r2 != null) {
                //            childNodes.add(r2);
                //            r2 = parseStar();
                //        }
                //
                //        AstNode r3 = parseStar();
                //        while (r3 != null) {
                //            childNodes.add(r3);
                //            r3 = parseStar();
                //        }

                when (qr.quantifier) {
                    QuantifiedRule.Quantifier.MAYBE_ONE -> {
                        out.println("        if (r$r_idx != null) {")
                        out.println("            childNodes.$addMethod(r$r_idx);")
                        out.println("        }")
                    }
                    QuantifiedRule.Quantifier.ZERO_OR_MORE -> {
                        out.println("        while (r$r_idx != null) {")
                        out.println("            childNodes.$addMethod(r$r_idx);")
                        out.println("            r$r_idx = $parseMethod();")
                        out.println("        }")
                    }
                    QuantifiedRule.Quantifier.ONE -> {
                        out.println("        if (r$r_idx == null) {")
                        out.println("            tokenStream.rollback(marker);")
                        out.println("            return null;")
                        out.println("        } else {")
                        out.println("            childNodes.$addMethod(r$r_idx);")
                        out.println("        }")
                    }
                    QuantifiedRule.Quantifier.ONE_OR_MORE -> {
                        out.println("        if (r$r_idx == null) {")
                        out.println("            tokenStream.rollback(marker);")
                        out.println("            return null;")
                        out.println("        }")
                        out.println("        while (r$r_idx != null) {")
                        out.println("            childNodes.$addMethod(r$r_idx);")
                        out.println("            r$r_idx = $parseMethod();")
                        out.println("        }")
                    }
                }
            }
            out.println()
            if (returnType == "List<AstNode>") {
                // nested Parse Method
                out.println("       return childNodes;")
            } else {
                out.println("       return new $returnType(childNodes);")
            }
            out.println("    }")

            nestedRules.forEach {
                generateNestedParseMethod(out, it)
            }
        }
    }
}

class NestedRuleInfo(val methodName: String, val variants: MutableList<RuleVariant>)
