package com.lazornet.grammar.lex.parsergenerator;

import com.lazornet.grammar.lazorlex.grammar.GrammarLexer;
import com.lazornet.grammar.lazorlex.grammar.GrammarParseException;
import com.lazornet.grammar.lazorlex.grammar.GrammarParser;
import com.lazornet.grammar.lazorlex.grammar.ProductionRules;
import com.lazornet.grammar.lex.lexergenerator.LexerGenerator;
import com.lazornet.grammar.lex.util.PackageUtil;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Generate a parser for the given BNF grammar file
 */
public class ParserGenerator {
    private final String packageDirectory;
    private final String packageName;
    private final File grammarFile;
    private final Log log;
    private final String strippedFileName;

    public ParserGenerator(File outputDirectory, String packageName, File grammarFile, Log log) {
        this.packageName = packageName;
        this.grammarFile = grammarFile;
        this.log = log;
        this.packageDirectory = PackageUtil.getPackageDirectory(outputDirectory, packageName);
        strippedFileName = LexerGenerator.stripFileName(grammarFile);
    }

    public void generateParser() throws MojoExecutionException {
        final String text;
        try {
            text = readText(grammarFile);
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to read the grammar file " + grammarFile.getName(), e);
        }
        final ProductionRules productionRules;
        try {
            productionRules = GrammarParser.parse(GrammarLexer.lexAllTokens(text), text);
        } catch (GrammarParseException ex) {
            log.error("Parse error in " + grammarFile.getName());
            log.error(ex.getMessage());
            throw new MojoExecutionException("Parse error in " + grammarFile.getName() + ": " + ex.getMessage(), ex);
        }
        final GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        try {
            generateFiles(grammarDefinition);
        } catch (IOException e) {
            throw new MojoExecutionException("IO error trying to write generated files for " + grammarFile.getName(), e);
        }
    }

    private void generateFiles(GrammarDefinition grammarDefinition) throws IOException {
        // ensure parent directory exists
        //noinspection ResultOfMethodCallIgnored
        new File(packageDirectory).mkdirs();

        generateAstNodeClass();
        generateAstNodes(grammarDefinition);
        generateTokenStream();
        generateParseException();
        generateParserClass(grammarDefinition);
    }

    private void generateParserClass(GrammarDefinition grammarDefinition) throws IOException {
        String className = strippedFileName + "Parser";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new ParserClassGenerator(packageName, strippedFileName, grammarDefinition).generate(out);

        out.close();
    }

    private void generateTokenStream() throws IOException {
        String className = strippedFileName + "TokenStream";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new TokenStreamClassGenerator(packageName, strippedFileName).generate(out);

        out.close();
    }

    private void generateParseException() throws IOException {
        String className = strippedFileName + "ParseException";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new ParseExceptionClassGenerator(packageName, strippedFileName).generate(out);

        out.close();
    }

    private void generateAstNodes(GrammarDefinition grammarDefinition) throws IOException {
        // Token nodes (leaves of the tree)
        for (String tokenName : grammarDefinition.getTokenNames()) {
            String className = tokenClassName(tokenName);
            PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

            new TokenNodeClassGenerator(packageName, tokenName, className).generate(out);

            out.close();
        }
        // Parent nodes (named production rules)
        for (NamedProductionRule namedRule : grammarDefinition.getNamedRules()) {
            String ruleName = namedRule.getName();
            // Upper case the first char eg "fooBar" -> "FooBar"
            String className = ruleName.substring(0, 1).toUpperCase() + ruleName.substring(1);
            PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

            new ParentNodeClassGenerator(packageName, className, namedRule).generate(out);

            out.close();
        }
    }

    static String tokenClassName(String tokenName) {
        final StringBuilder sb = new StringBuilder();
        boolean upperCase = true;

        for (char ch : tokenName.toCharArray()) {
            if (ch == '_') {
                upperCase = true;
            } else {
                if (upperCase) {
                    sb.append(Character.toUpperCase(ch));
                    upperCase = false;
                } else {
                    sb.append(Character.toLowerCase(ch));
                }
            }
        }

        return sb.toString();
    }


    private void generateAstNodeClass() throws IOException {
        String className = "AstNode";
        PrintWriter out = new PrintWriter(getFileWriter(className + ".java"));

        new AstNodeClassGenerator(packageName).generate(out);

        out.close();
    }

    private FileWriter getFileWriter(String className) throws IOException {
        return new FileWriter(new File(packageDirectory, className));
    }

    private String readText(File lexFile) throws IOException {
        StringBuilder sb = new StringBuilder();
        Files.lines( Paths.get(lexFile.getAbsolutePath()), StandardCharsets.UTF_8)
                .forEach(s -> sb.append(s).append("\n"));
        return sb.toString();
    }
}
