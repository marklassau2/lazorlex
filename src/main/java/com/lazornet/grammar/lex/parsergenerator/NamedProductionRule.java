package com.lazornet.grammar.lex.parsergenerator;

import java.util.List;

public final class NamedProductionRule {
    private final String name;
    private final List<RuleVariant> variants;
    private final CardinalityMap cardinalityMap;

    public NamedProductionRule(String name, List<RuleVariant> variants, CardinalityMap cardinalityMap) {
        this.name = name;
        this.variants = variants;
        this.cardinalityMap = cardinalityMap;
    }

    public String getName() {
        return name;
    }

    public CardinalityMap getCardinalityMap() {
        return cardinalityMap;
    }

    public List<RuleVariant> getVariants() {
        return variants;
    }

    @Override
    public String toString() {
        return "ProductionRule{" + name + ": variants=" + variants +
                ", cardinalityMap=" + cardinalityMap +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NamedProductionRule)) return false;

        NamedProductionRule that = (NamedProductionRule) o;

        if (!name.equals(that.name)) return false;
        if (!variants.equals(that.variants)) return false;
        return cardinalityMap.equals(that.cardinalityMap);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + variants.hashCode();
        result = 31 * result + cardinalityMap.hashCode();
        return result;
    }
}
