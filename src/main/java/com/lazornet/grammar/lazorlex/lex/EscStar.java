package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_STAR token.
 */
public class EscStar extends AstNode.TokenNode {

    public EscStar(String text) {
        super(text);
    }
}
