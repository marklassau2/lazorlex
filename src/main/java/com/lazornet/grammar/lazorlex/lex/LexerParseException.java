package com.lazornet.grammar.lazorlex.lex;

public class LexerParseException extends RuntimeException {
    public LexerParseException(String message) {
        super(message);
    }
}
