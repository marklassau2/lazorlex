package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the DOT token.
 */
public class Dot extends AstNode.TokenNode {

    public Dot(String text) {
        super(text);
    }
}
