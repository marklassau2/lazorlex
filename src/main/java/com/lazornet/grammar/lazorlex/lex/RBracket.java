package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the R_BRACKET token.
 */
public class RBracket extends AstNode.TokenNode {

    public RBracket(String text) {
        super(text);
    }
}
