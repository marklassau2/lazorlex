package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the TOKEN_NAME token.
 */
public class TokenName extends AstNode.TokenNode {

    public TokenName(String text) {
        super(text);
    }
}
