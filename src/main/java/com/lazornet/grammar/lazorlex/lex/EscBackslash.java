package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_BACKSLASH token.
 */
public class EscBackslash extends AstNode.TokenNode {

    public EscBackslash(String text) {
        super(text);
    }
}
