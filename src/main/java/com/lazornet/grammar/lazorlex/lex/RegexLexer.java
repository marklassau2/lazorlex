package com.lazornet.grammar.lazorlex.lex;

import javax.annotation.Generated;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This parent node represents an outcome of the regexLexer production rule
 *
 * This class was automatically generated by Lazorlex.
 */
@Generated(value="com.lazornet.grammar.lazorlex")
public class RegexLexer extends AstNode.ParentNode {
    private final Regex _regex;
    private final List<Slash> _slashs;

    public RegexLexer(List<AstNode> childNodes) {
        super(childNodes);

        Regex _regex = null;
        List<Slash> _slashs = new LinkedList<>();

        for (AstNode childNode : childNodes) {
            if (childNode instanceof Regex) {
                _regex = (Regex) childNode;
            } else if (childNode instanceof Slash) {
                _slashs.add((Slash) childNode);
            } else {
                throw new IllegalStateException("Illegal type of child node: " + childNode.getClass());
            }
        }

        this._regex = _regex;
        this._slashs = Collections.unmodifiableList(_slashs);
    }

    /**
     * Returns the Regex value - guaranteed not null.
     */
    public Regex getRegex() {
        return _regex;
    }

    /**
     * Returns a List of Slash values.
     */
    public List<Slash> getSlashs() {
        return _slashs;
    }

}
