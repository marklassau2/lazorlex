package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_LF token.
 */
public class EscLf extends AstNode.TokenNode {

    public EscLf(String text) {
        super(text);
    }
}
