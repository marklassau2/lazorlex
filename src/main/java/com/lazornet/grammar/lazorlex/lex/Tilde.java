package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the TILDE token.
 */
public class Tilde extends AstNode.TokenNode {

    public Tilde(String text) {
        super(text);
    }
}
