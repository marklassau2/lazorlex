package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_TAB token.
 */
public class EscTab extends AstNode.TokenNode {

    public EscTab(String text) {
        super(text);
    }
}
