package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_PLUS token.
 */
public class EscPlus extends AstNode.TokenNode {

    public EscPlus(String text) {
        super(text);
    }
}
