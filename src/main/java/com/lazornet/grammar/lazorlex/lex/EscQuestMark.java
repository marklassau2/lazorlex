package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_QUEST_MARK token.
 */
public class EscQuestMark extends AstNode.TokenNode {

    public EscQuestMark(String text) {
        super(text);
    }
}
