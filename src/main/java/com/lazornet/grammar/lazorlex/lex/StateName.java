package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the STATE_NAME token.
 */
public class StateName extends AstNode.TokenNode {

    public StateName(String text) {
        super(text);
    }
}
