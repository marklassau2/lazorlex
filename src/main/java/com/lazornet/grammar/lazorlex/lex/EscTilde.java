package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_TILDE token.
 */
public class EscTilde extends AstNode.TokenNode {

    public EscTilde(String text) {
        super(text);
    }
}
