package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the TEXT token.
 */
public class Text extends AstNode.TokenNode {

    public Text(String text) {
        super(text);
    }
}
