package com.lazornet.grammar.lazorlex.lex;

import javax.annotation.Generated;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This parent node represents an outcome of the stateDefinition production rule
 *
 * This class was automatically generated by Lazorlex.
 */
@Generated(value="com.lazornet.grammar.lazorlex")
public class StateDefinition extends AstNode.ParentNode {
    private final StateName _stateName;
    private final List<RuleDefinition> _ruleDefinitions;
    private final RBrace _rBrace;
    private final LBrace _lBrace;

    public StateDefinition(List<AstNode> childNodes) {
        super(childNodes);

        StateName _stateName = null;
        List<RuleDefinition> _ruleDefinitions = new LinkedList<>();
        RBrace _rBrace = null;
        LBrace _lBrace = null;

        for (AstNode childNode : childNodes) {
            if (childNode instanceof StateName) {
                _stateName = (StateName) childNode;
            } else if (childNode instanceof RuleDefinition) {
                _ruleDefinitions.add((RuleDefinition) childNode);
            } else if (childNode instanceof RBrace) {
                _rBrace = (RBrace) childNode;
            } else if (childNode instanceof LBrace) {
                _lBrace = (LBrace) childNode;
            } else {
                throw new IllegalStateException("Illegal type of child node: " + childNode.getClass());
            }
        }

        this._stateName = _stateName;
        this._ruleDefinitions = Collections.unmodifiableList(_ruleDefinitions);
        this._rBrace = _rBrace;
        this._lBrace = _lBrace;
    }

    /**
     * Returns the StateName value - guaranteed not null.
     */
    public StateName getStateName() {
        return _stateName;
    }

    /**
     * Returns a List of RuleDefinition values.
     */
    public List<RuleDefinition> getRuleDefinitions() {
        return _ruleDefinitions;
    }

    /**
     * Returns the RBrace value - guaranteed not null.
     */
    public RBrace getRBrace() {
        return _rBrace;
    }

    /**
     * Returns the LBrace value - guaranteed not null.
     */
    public LBrace getLBrace() {
        return _lBrace;
    }

}
