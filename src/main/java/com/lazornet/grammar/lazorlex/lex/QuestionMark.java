package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the QUESTION_MARK token.
 */
public class QuestionMark extends AstNode.TokenNode {

    public QuestionMark(String text) {
        super(text);
    }
}
