package com.lazornet.grammar.lazorlex.lex;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class LexLexer {
    private static final TokenMatcher matcher0 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch >= 'A' && ch <= 'Z' || ch == '_') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch >= 'A' && ch <= 'Z' || ch == '_' || ch >= '0' && ch <= '9') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher1 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch >= 'a' && ch <= 'z') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch >= 'a' && ch <= 'z' || ch == '_' || ch >= '0' && ch <= '9') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher2 = charStream -> {
        // Exact text '{'
        char ch;
        ch = charStream.read();
        if (ch != '{') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher3 = charStream -> {
        // Exact text '}'
        char ch;
        ch = charStream.read();
        if (ch != '}') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher4 = charStream -> {
        // Exact text '/'
        char ch;
        ch = charStream.read();
        if (ch != '/') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher5 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == '"') {
            count++;
        } else {
            return -1;
        }
        return count;
    };
    private static final TokenMatcher matcher6 = charStream -> {
        // Exact text '->'
        char ch;
        ch = charStream.read();
        if (ch != '-') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '>') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher7 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher8 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == '#') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (!(ch == '\r' || ch == '\n' || ch == (char) 0)) {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher9 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == '"') {
            count++;
        } else {
            return -1;
        }
        return count;
    };
    private static final TokenMatcher matcher10 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch != '"' && ch != (char) 0) {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch != '"' && ch != (char) 0) {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher11 = charStream -> {
        // Exact text '/'
        char ch;
        ch = charStream.read();
        if (ch != '/') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher12 = charStream -> {
        // Exact text '['
        char ch;
        ch = charStream.read();
        if (ch != '[') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher13 = charStream -> {
        // Exact text '~'
        char ch;
        ch = charStream.read();
        if (ch != '~') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher14 = charStream -> {
        // Exact text '.'
        char ch;
        ch = charStream.read();
        if (ch != '.') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher15 = charStream -> {
        // Exact text '*'
        char ch;
        ch = charStream.read();
        if (ch != '*') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher16 = charStream -> {
        // Exact text '+'
        char ch;
        ch = charStream.read();
        if (ch != '+') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher17 = charStream -> {
        // Exact text '?'
        char ch;
        ch = charStream.read();
        if (ch != '?') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher18 = charStream -> {
        // Exact text '\\'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher19 = charStream -> {
        // Exact text '\*'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '*') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher20 = charStream -> {
        // Exact text '\+'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '+') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher21 = charStream -> {
        // Exact text '\?'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '?') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher22 = charStream -> {
        // Exact text '\~'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '~') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher23 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch > (char) 0) {
            count++;
        } else {
            return -1;
        }
        return count;
    };
    private static final TokenMatcher matcher24 = charStream -> {
        // Exact text ']'
        char ch;
        ch = charStream.read();
        if (ch != ']') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher25 = charStream -> {
        // Exact text '-'
        char ch;
        ch = charStream.read();
        if (ch != '-') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher26 = charStream -> {
        // Exact text '\\'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher27 = charStream -> {
        // Exact text '\t'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != 't') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher28 = charStream -> {
        // Exact text '\r'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != 'r') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher29 = charStream -> {
        // Exact text '\n'
        char ch;
        ch = charStream.read();
        if (ch != '\\') {
            return -1;
        }
        ch = charStream.read();
        if (ch != 'n') {
            return -1;
        }
        return 2;
    };
    private static final TokenMatcher matcher30 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch > (char) 0) {
            count++;
        } else {
            return -1;
        }
        return count;
    };

    private static final LexerState[] states = {
            new LexerState("init", asList(
                    new LexerRule(matcher0, LexToken.Type.TOKEN_NAME, -1),
                    new LexerRule(matcher1, LexToken.Type.STATE_NAME, -1),
                    new LexerRule(matcher2, LexToken.Type.L_BRACE, -1),
                    new LexerRule(matcher3, LexToken.Type.R_BRACE, -1),
                    new LexerRule(matcher4, LexToken.Type.SLASH, 2),
                    new LexerRule(matcher5, LexToken.Type.DOUBLE_QUOTE, 1),
                    new LexerRule(matcher6, LexToken.Type.R_ARROW, -1),
                    new LexerRule(matcher7, LexToken.Type._WHITESPACE, -1),
                    new LexerRule(matcher8, LexToken.Type._COMMENT, -1)
            )),
            new LexerState("exact_text", asList(
                    new LexerRule(matcher9, LexToken.Type.DOUBLE_QUOTE, 0),
                    new LexerRule(matcher10, LexToken.Type.TEXT, -1)
            )),
            new LexerState("regex", asList(
                    new LexerRule(matcher11, LexToken.Type.SLASH, 0),
                    new LexerRule(matcher12, LexToken.Type.L_BRACKET, 3),
                    new LexerRule(matcher13, LexToken.Type.TILDE, -1),
                    new LexerRule(matcher14, LexToken.Type.DOT, -1),
                    new LexerRule(matcher15, LexToken.Type.STAR, -1),
                    new LexerRule(matcher16, LexToken.Type.PLUS, -1),
                    new LexerRule(matcher17, LexToken.Type.QUESTION_MARK, -1),
                    new LexerRule(matcher18, LexToken.Type.ESC_BACKSLASH, -1),
                    new LexerRule(matcher19, LexToken.Type.ESC_STAR, -1),
                    new LexerRule(matcher20, LexToken.Type.ESC_PLUS, -1),
                    new LexerRule(matcher21, LexToken.Type.ESC_QUEST_MARK, -1),
                    new LexerRule(matcher22, LexToken.Type.ESC_TILDE, -1),
                    new LexerRule(matcher23, LexToken.Type.CHAR, -1)
            )),
            new LexerState("regex_alt", asList(
                    new LexerRule(matcher24, LexToken.Type.R_BRACKET, 2),
                    new LexerRule(matcher25, LexToken.Type.DASH, -1),
                    new LexerRule(matcher26, LexToken.Type.ESC_BACKSLASH, -1),
                    new LexerRule(matcher27, LexToken.Type.ESC_TAB, -1),
                    new LexerRule(matcher28, LexToken.Type.ESC_CR, -1),
                    new LexerRule(matcher29, LexToken.Type.ESC_LF, -1),
                    new LexerRule(matcher30, LexToken.Type.CHAR, -1)
            )),
    };

    private LexingCharStream charStream;
    private int state = 0;
    private LexToken.Type tokenType;
    private int tokenEnd = -1;

    public static List<LexToken> lexAllTokens(CharSequence text) {
        final List<LexToken> tokens = new LinkedList<>();
        LexLexer lexer = new LexLexer();
        lexer.start(text);
        while (lexer.getTokenType() != null) {
            if (!lexer.getTokenType().name().startsWith("_")) {
                tokens.add(new LexToken(lexer.getTokenType(), lexer.getTokenStart(), lexer.getTokenText()));
            }
            lexer.advance();
        }
        return tokens;
    }

    public void start(CharSequence buffer) {
        reset(buffer, 0, 0);
    }

    public void reset(CharSequence buffer, int startOffset, int initialState) {
        this.charStream = new LexingCharStream(buffer, startOffset);
        this.state = initialState;
        this.tokenEnd = startOffset;

        readToken();
    }

    public void readToken() {
        if (tokenEnd >= charStream.length()) {
            tokenType = null;
            return;
        }

        int bestLength = 0;
        LexerRule bestMatch = null;
        for (LexerRule rule : states[state].getRules()) {
            charStream.rewind();
            int matchLength = rule.match(charStream);
            if (matchLength > bestLength) {
                bestLength = matchLength;
                bestMatch = rule;
            }
        }
        if (bestMatch == null) {
            throw new IllegalStateException("syntax error: no matching token found at " +
                    charStream.getTokenStart() + " (state: " + states[state].name + ")\n");
        }
        if (bestMatch.hasNextState()) {
            state = bestMatch.nextState;
        }
        tokenEnd = charStream.getTokenStart() + bestLength;
        tokenType = bestMatch.tokenType;
    }

    public void advance() {
        charStream.setTokenStart(tokenEnd);
        readToken();
    }

    public int getState() {
        return state;
    }

    public int getTokenEnd() {
        return tokenEnd;
    }

    public LexToken.Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return charStream.getTokenStart();
    }

    public String getTokenText() {
        return charStream.getTextBuffer().subSequence(getTokenStart(), tokenEnd).toString();
    }

    private static class LexerState {
        private final String name;
        private final Iterable<LexerRule> rules;

        private LexerState(String name, Iterable<LexerRule> rules) {
            this.name = name;
            this.rules = rules;
        }

        private Iterable<LexerRule> getRules() {
            return rules;
        }
    }

    private static class LexerRule {
        private final TokenMatcher tokenMatcher;
        private final int nextState;
        private final LexToken.Type tokenType;

        private LexerRule(TokenMatcher tokenMatcher, LexToken.Type tokenType, int nextState) {
            this.tokenMatcher = tokenMatcher;
            this.tokenType = tokenType;
            this.nextState = nextState;
        }

        private int match(LexingCharStream charStream) {
            return tokenMatcher.match(charStream);
        }

        private boolean hasNextState() {
            return nextState >= 0;
        }
    }

    interface TokenMatcher {
        int match(LexingCharStream charStream);
    }
}
