package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the SLASH token.
 */
public class Slash extends AstNode.TokenNode {

    public Slash(String text) {
        super(text);
    }
}
