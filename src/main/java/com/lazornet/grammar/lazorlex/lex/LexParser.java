package com.lazornet.grammar.lazorlex.lex;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class LexParser {
    private final LexTokenStream tokenStream;
    private final CharSequence text;
    private final FailRegister failRegister = new FailRegister();

    /**
     * Parse the given list of tokens.
     * 
     * @param tokens the tokens emitted by the Lexer
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List, CharSequence) 
     */
    public static Statements parse(List<LexToken> tokens) throws LexParseException {
        return new LexParser(tokens, null).parse();
    }

    /**
     * Parse the given list of tokens.
     * <p>
     * This version of <pre>parse()</pre> accepts the original text to be parsed as a param. This is purely to be used
     * to in error handling to calculate the error position as a line number + column number. If the original text is 
     * not provided then the character position in the stream is displayed instead.
     *
     * @param tokens the tokens emitted by the Lexer
     * @param text the original text that was tokenised - this is only used for error messaging              
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List)
     */
    public static Statements parse(List<LexToken> tokens, CharSequence text) throws LexParseException {
        return new LexParser(tokens, text).parse();
    }

    private Statements parse() throws LexParseException {
        Statements result = parseStatements();
        if (result != null && tokenStream.isEof()) {
            return result;
        } else {
            throw new LexParseException(tokenStream.getCharIndex(failRegister.depth), failRegister.expectedTokens, text);
        }
    }

    private LexParser(List<LexToken> tokens, CharSequence text) {
        this.tokenStream = new LexTokenStream(tokens);
        this.text = text;
    }

    private Statements parseStatements() {
        return parseStatements_v0();
    }

    private Statements parseStatements_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseStatement();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        }
        while (r0 != null) {
            childNodes.add(r0);
            r0 = parseStatement();
        }

       return new Statements(childNodes);
    }

    private Statement parseStatement() {
        Statement result;
        result = parseStatement_v0();
        if (result != null) return result;
        return parseStatement_v1();
    }

    private Statement parseStatement_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseStateDefinition();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new Statement(childNodes);
    }

    private Statement parseStatement_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRuleDefinition();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new Statement(childNodes);
    }

    private StateDefinition parseStateDefinition() {
        return parseStateDefinition_v0();
    }

    private StateDefinition parseStateDefinition_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseStateName();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseLBrace();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseRuleDefinition();
        while (r2 != null) {
            childNodes.add(r2);
            r2 = parseRuleDefinition();
        }
        AstNode r3 = parseRBrace();
        if (r3 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r3);
        }

       return new StateDefinition(childNodes);
    }

    private RuleDefinition parseRuleDefinition() {
        return parseRuleDefinition_v0();
    }

    private RuleDefinition parseRuleDefinition_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseTokenName();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseTokenLexer();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        List<AstNode> r2 = parseRuleDefinition_v0_r2();
        if (r2 != null) {
            childNodes.addAll(r2);
        }

       return new RuleDefinition(childNodes);
    }

    private List<AstNode> parseRuleDefinition_v0_r2() {
        return parseRuleDefinition_v0_r2_v0();
    }

    private List<AstNode> parseRuleDefinition_v0_r2_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRArrow();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseStateName();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }

       return childNodes;
    }

    private TokenLexer parseTokenLexer() {
        TokenLexer result;
        result = parseTokenLexer_v0();
        if (result != null) return result;
        return parseTokenLexer_v1();
    }

    private TokenLexer parseTokenLexer_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRegexLexer();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new TokenLexer(childNodes);
    }

    private TokenLexer parseTokenLexer_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseExactTextLexer();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new TokenLexer(childNodes);
    }

    private ExactTextLexer parseExactTextLexer() {
        return parseExactTextLexer_v0();
    }

    private ExactTextLexer parseExactTextLexer_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseDoubleQuote();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseText();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseDoubleQuote();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }

       return new ExactTextLexer(childNodes);
    }

    private RegexLexer parseRegexLexer() {
        return parseRegexLexer_v0();
    }

    private RegexLexer parseRegexLexer_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseSlash();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseRegex();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseSlash();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }

       return new RegexLexer(childNodes);
    }

    private Regex parseRegex() {
        return parseRegex_v0();
    }

    private Regex parseRegex_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRegexChunk();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        }
        while (r0 != null) {
            childNodes.add(r0);
            r0 = parseRegexChunk();
        }

       return new Regex(childNodes);
    }

    private RegexChunk parseRegexChunk() {
        return parseRegexChunk_v0();
    }

    private RegexChunk parseRegexChunk_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseTilde();
        if (r0 != null) {
            childNodes.add(r0);
        }
        AstNode r1 = parseRegexAtom();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseRegexQuantifier();
        if (r2 != null) {
            childNodes.add(r2);
        }

       return new RegexChunk(childNodes);
    }

    private RegexAtom parseRegexAtom() {
        RegexAtom result;
        result = parseRegexAtom_v0();
        if (result != null) return result;
        result = parseRegexAtom_v1();
        if (result != null) return result;
        result = parseRegexAtom_v2();
        if (result != null) return result;
        return parseRegexAtom_v3();
    }

    private RegexAtom parseRegexAtom_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseDot();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAtom(childNodes);
    }

    private RegexAtom parseRegexAtom_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseChar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAtom(childNodes);
    }

    private RegexAtom parseRegexAtom_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRegexAlt();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAtom(childNodes);
    }

    private RegexAtom parseRegexAtom_v3() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRegexEscape();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAtom(childNodes);
    }

    private RegexEscape parseRegexEscape() {
        RegexEscape result;
        result = parseRegexEscape_v0();
        if (result != null) return result;
        result = parseRegexEscape_v1();
        if (result != null) return result;
        result = parseRegexEscape_v2();
        if (result != null) return result;
        result = parseRegexEscape_v3();
        if (result != null) return result;
        return parseRegexEscape_v4();
    }

    private RegexEscape parseRegexEscape_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscBackslash();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexEscape(childNodes);
    }

    private RegexEscape parseRegexEscape_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscStar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexEscape(childNodes);
    }

    private RegexEscape parseRegexEscape_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscPlus();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexEscape(childNodes);
    }

    private RegexEscape parseRegexEscape_v3() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscQuestMark();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexEscape(childNodes);
    }

    private RegexEscape parseRegexEscape_v4() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscTilde();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexEscape(childNodes);
    }

    private RegexQuantifier parseRegexQuantifier() {
        RegexQuantifier result;
        result = parseRegexQuantifier_v0();
        if (result != null) return result;
        result = parseRegexQuantifier_v1();
        if (result != null) return result;
        return parseRegexQuantifier_v2();
    }

    private RegexQuantifier parseRegexQuantifier_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseStar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexQuantifier(childNodes);
    }

    private RegexQuantifier parseRegexQuantifier_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parsePlus();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexQuantifier(childNodes);
    }

    private RegexQuantifier parseRegexQuantifier_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseQuestionMark();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexQuantifier(childNodes);
    }

    private RegexAlt parseRegexAlt() {
        return parseRegexAlt_v0();
    }

    private RegexAlt parseRegexAlt_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseLBracket();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseRegexAltChunk();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        }
        while (r1 != null) {
            childNodes.add(r1);
            r1 = parseRegexAltChunk();
        }
        AstNode r2 = parseRBracket();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }

       return new RegexAlt(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk() {
        RegexAltChunk result;
        result = parseRegexAltChunk_v0();
        if (result != null) return result;
        result = parseRegexAltChunk_v1();
        if (result != null) return result;
        result = parseRegexAltChunk_v2();
        if (result != null) return result;
        result = parseRegexAltChunk_v3();
        if (result != null) return result;
        result = parseRegexAltChunk_v4();
        if (result != null) return result;
        return parseRegexAltChunk_v5();
    }

    private RegexAltChunk parseRegexAltChunk_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRegexAltRange();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseChar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscBackslash();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk_v3() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscTab();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk_v4() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscCr();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltChunk parseRegexAltChunk_v5() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseEscLf();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new RegexAltChunk(childNodes);
    }

    private RegexAltRange parseRegexAltRange() {
        return parseRegexAltRange_v0();
    }

    private RegexAltRange parseRegexAltRange_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        LexTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseChar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseDash();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseChar();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }

       return new RegexAltRange(childNodes);
    }
    private StateName parseStateName() {
        if (tokenStream.tokenType() == LexToken.Type.STATE_NAME) {
            return new StateName(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.STATE_NAME);
            return null;
        }
    }
    private RBrace parseRBrace() {
        if (tokenStream.tokenType() == LexToken.Type.R_BRACE) {
            return new RBrace(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.R_BRACE);
            return null;
        }
    }
    private DoubleQuote parseDoubleQuote() {
        if (tokenStream.tokenType() == LexToken.Type.DOUBLE_QUOTE) {
            return new DoubleQuote(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.DOUBLE_QUOTE);
            return null;
        }
    }
    private TokenName parseTokenName() {
        if (tokenStream.tokenType() == LexToken.Type.TOKEN_NAME) {
            return new TokenName(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.TOKEN_NAME);
            return null;
        }
    }
    private EscTab parseEscTab() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_TAB) {
            return new EscTab(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_TAB);
            return null;
        }
    }
    private Char parseChar() {
        if (tokenStream.tokenType() == LexToken.Type.CHAR) {
            return new Char(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.CHAR);
            return null;
        }
    }
    private LBracket parseLBracket() {
        if (tokenStream.tokenType() == LexToken.Type.L_BRACKET) {
            return new LBracket(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.L_BRACKET);
            return null;
        }
    }
    private Text parseText() {
        if (tokenStream.tokenType() == LexToken.Type.TEXT) {
            return new Text(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.TEXT);
            return null;
        }
    }
    private EscLf parseEscLf() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_LF) {
            return new EscLf(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_LF);
            return null;
        }
    }
    private Dash parseDash() {
        if (tokenStream.tokenType() == LexToken.Type.DASH) {
            return new Dash(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.DASH);
            return null;
        }
    }
    private EscPlus parseEscPlus() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_PLUS) {
            return new EscPlus(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_PLUS);
            return null;
        }
    }
    private QuestionMark parseQuestionMark() {
        if (tokenStream.tokenType() == LexToken.Type.QUESTION_MARK) {
            return new QuestionMark(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.QUESTION_MARK);
            return null;
        }
    }
    private EscTilde parseEscTilde() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_TILDE) {
            return new EscTilde(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_TILDE);
            return null;
        }
    }
    private LBrace parseLBrace() {
        if (tokenStream.tokenType() == LexToken.Type.L_BRACE) {
            return new LBrace(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.L_BRACE);
            return null;
        }
    }
    private EscBackslash parseEscBackslash() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_BACKSLASH) {
            return new EscBackslash(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_BACKSLASH);
            return null;
        }
    }
    private Slash parseSlash() {
        if (tokenStream.tokenType() == LexToken.Type.SLASH) {
            return new Slash(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.SLASH);
            return null;
        }
    }
    private Dot parseDot() {
        if (tokenStream.tokenType() == LexToken.Type.DOT) {
            return new Dot(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.DOT);
            return null;
        }
    }
    private RArrow parseRArrow() {
        if (tokenStream.tokenType() == LexToken.Type.R_ARROW) {
            return new RArrow(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.R_ARROW);
            return null;
        }
    }
    private RBracket parseRBracket() {
        if (tokenStream.tokenType() == LexToken.Type.R_BRACKET) {
            return new RBracket(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.R_BRACKET);
            return null;
        }
    }
    private EscStar parseEscStar() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_STAR) {
            return new EscStar(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_STAR);
            return null;
        }
    }
    private EscQuestMark parseEscQuestMark() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_QUEST_MARK) {
            return new EscQuestMark(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_QUEST_MARK);
            return null;
        }
    }
    private Star parseStar() {
        if (tokenStream.tokenType() == LexToken.Type.STAR) {
            return new Star(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.STAR);
            return null;
        }
    }
    private EscCr parseEscCr() {
        if (tokenStream.tokenType() == LexToken.Type.ESC_CR) {
            return new EscCr(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.ESC_CR);
            return null;
        }
    }
    private Tilde parseTilde() {
        if (tokenStream.tokenType() == LexToken.Type.TILDE) {
            return new Tilde(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.TILDE);
            return null;
        }
    }
    private Plus parsePlus() {
        if (tokenStream.tokenType() == LexToken.Type.PLUS) {
            return new Plus(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), LexToken.Type.PLUS);
            return null;
        }
    }

    private static final class FailRegister {
        private int depth = -1;
        private final HashSet<LexToken.Type> expectedTokens = new HashSet<>();

        public void registerFail(int index, LexToken.Type expectedToken) {
            if (index == depth) {
                expectedTokens.add(expectedToken);
            } else if (index > depth) {
                depth = index;
                expectedTokens.clear();
                expectedTokens.add(expectedToken);
            }
        }
    }
}    
