package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the R_BRACE token.
 */
public class RBrace extends AstNode.TokenNode {

    public RBrace(String text) {
        super(text);
    }
}
