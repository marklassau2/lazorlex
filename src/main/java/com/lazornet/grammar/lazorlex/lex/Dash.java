package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the DASH token.
 */
public class Dash extends AstNode.TokenNode {

    public Dash(String text) {
        super(text);
    }
}
