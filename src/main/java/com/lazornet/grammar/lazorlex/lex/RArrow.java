package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the R_ARROW token.
 */
public class RArrow extends AstNode.TokenNode {

    public RArrow(String text) {
        super(text);
    }
}
