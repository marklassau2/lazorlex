package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the L_BRACKET token.
 */
public class LBracket extends AstNode.TokenNode {

    public LBracket(String text) {
        super(text);
    }
}
