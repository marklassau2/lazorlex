package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the CHAR token.
 */
public class Char extends AstNode.TokenNode {

    public Char(String text) {
        super(text);
    }
}
