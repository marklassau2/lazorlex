package com.lazornet.grammar.lazorlex.lex;

import javax.annotation.Generated;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This parent node represents an outcome of the regex production rule
 *
 * This class was automatically generated by Lazorlex.
 */
@Generated(value="com.lazornet.grammar.lazorlex")
public class Regex extends AstNode.ParentNode {
    private final List<RegexChunk> _regexChunks;

    public Regex(List<AstNode> childNodes) {
        super(childNodes);

        List<RegexChunk> _regexChunks = new LinkedList<>();

        for (AstNode childNode : childNodes) {
            if (childNode instanceof RegexChunk) {
                _regexChunks.add((RegexChunk) childNode);
            } else {
                throw new IllegalStateException("Illegal type of child node: " + childNode.getClass());
            }
        }

        this._regexChunks = Collections.unmodifiableList(_regexChunks);
    }

    /**
     * Returns a List of RegexChunk values.
     */
    public List<RegexChunk> getRegexChunks() {
        return _regexChunks;
    }

}
