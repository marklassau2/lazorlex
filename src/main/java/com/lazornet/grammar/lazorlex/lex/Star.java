package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the STAR token.
 */
public class Star extends AstNode.TokenNode {

    public Star(String text) {
        super(text);
    }
}
