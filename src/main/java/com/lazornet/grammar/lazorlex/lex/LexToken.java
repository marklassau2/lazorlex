package com.lazornet.grammar.lazorlex.lex;

public class LexToken {
    public enum Type {CHAR, DASH, DOT, DOUBLE_QUOTE, ESC_BACKSLASH, ESC_CR, ESC_LF, ESC_PLUS, ESC_QUEST_MARK, ESC_STAR,
        ESC_TAB, ESC_TILDE, L_BRACE, L_BRACKET, PLUS, QUESTION_MARK, R_ARROW, R_BRACE, R_BRACKET, SLASH, STAR,
        STATE_NAME, TEXT, TILDE, TOKEN_NAME, _COMMENT, _WHITESPACE}

    private final Type tokenType;
    private final int tokenStart;
    private final String text;

    public LexToken(Type tokenType, int tokenStart, String text) {
        this.tokenType = tokenType;
        this.tokenStart = tokenStart;
        this.text = text;
    }

    public Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return tokenStart;
    }

    public String getText() {
        return text;
    }
}
