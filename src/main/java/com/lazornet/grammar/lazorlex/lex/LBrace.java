package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the L_BRACE token.
 */
public class LBrace extends AstNode.TokenNode {

    public LBrace(String text) {
        super(text);
    }
}
