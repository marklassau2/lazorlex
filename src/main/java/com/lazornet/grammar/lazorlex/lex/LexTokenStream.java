package com.lazornet.grammar.lazorlex.lex;

import java.util.List;

public class LexTokenStream {
    private final List<LexToken> tokens;
    private int index = 0;

    public LexTokenStream(List<LexToken> tokens) {
        this.tokens = tokens;
    }

    public boolean isEof() {
        return index >= tokens.size();
    }

    public Marker mark() {
        return new Marker(index);
    }

    public void rollback(Marker marker) {
        index = marker.index;
    }

    public LexToken.Type tokenType() {
        if (isEof()) return null;
        return tokens.get(index).getTokenType();
    }

    public LexToken next() {
        if (isEof()) return null;
        return tokens.get(index++);
    }

    public int getIndex() {
        return index;
    }
    
    public int getCharIndex(int tokenIndex) {
        if (tokenIndex >= tokens.size()) {
            // EOF
            LexToken finalToken = tokens.get(tokens.size() - 1);
            return finalToken.getTokenStart() + finalToken.getText().length();
        } else {
            return tokens.get(tokenIndex).getTokenStart();
        }
    }
    
    static class Marker {
        private final int index;

        private Marker(int index) {
            this.index = index;
        }
    }
}

