package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the ESC_CR token.
 */
public class EscCr extends AstNode.TokenNode {

    public EscCr(String text) {
        super(text);
    }
}
