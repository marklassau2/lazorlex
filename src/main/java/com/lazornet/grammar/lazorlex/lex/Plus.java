package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the PLUS token.
 */
public class Plus extends AstNode.TokenNode {

    public Plus(String text) {
        super(text);
    }
}
