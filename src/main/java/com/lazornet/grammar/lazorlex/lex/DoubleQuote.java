package com.lazornet.grammar.lazorlex.lex;

/**
 * This node represents the DOUBLE_QUOTE token.
 */
public class DoubleQuote extends AstNode.TokenNode {

    public DoubleQuote(String text) {
        super(text);
    }
}
