package com.lazornet.grammar.lazorlex.grammar;

public class LexingCharStream {
    private final CharSequence text;
    private int tokenStart;
    private int idx;

    LexingCharStream(CharSequence buffer, int offset) {
        this.text = buffer;
        this.tokenStart = offset;
        this.idx = offset;
    }

    void rewind() {
        idx = tokenStart;
    }
    
    public char read() {
        if (idx >= text.length()) return (char) 0;
        return text.charAt(idx++);
    }

    int getTokenStart() {
        return tokenStart;
    }

    public void setTokenStart(int tokenStart) {
        this.tokenStart = tokenStart;
    }

    public int length() {
        return text.length();
    }

    CharSequence getTextBuffer() {
        return text;
    }
}
