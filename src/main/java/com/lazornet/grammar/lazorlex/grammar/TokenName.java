package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the TOKEN_NAME token.
 */
public class TokenName extends AstNode.TokenNode {

    public TokenName(String text) {
        super(text);
    }
}
