package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the R_PAREN token.
 */
public class RParen extends AstNode.TokenNode {

    public RParen(String text) {
        super(text);
    }
}
