package com.lazornet.grammar.lazorlex.grammar;

public class GrammarToken {
    public enum Type {COLON, L_PAREN, PIPE, PLUS, QUESTION_MARK, RULE_NAME, R_PAREN, SEMICOLON, STAR, TOKEN_NAME,
        _COMMENT, _WHITESPACE}

    private final Type tokenType;
    private final int tokenStart;
    private final String text;

    public GrammarToken(Type tokenType, int tokenStart, String text) {
        this.tokenType = tokenType;
        this.tokenStart = tokenStart;
        this.text = text;
    }

    public Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return tokenStart;
    }

    public String getText() {
        return text;
    }
}
