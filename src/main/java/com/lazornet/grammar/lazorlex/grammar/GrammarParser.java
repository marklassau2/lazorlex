package com.lazornet.grammar.lazorlex.grammar;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class GrammarParser {
    private final GrammarTokenStream tokenStream;
    private final CharSequence text;
    private final FailRegister failRegister = new FailRegister();

    /**
     * Parse the given list of tokens.
     * 
     * @param tokens the tokens emitted by the Lexer
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List, CharSequence) 
     */
    public static ProductionRules parse(List<GrammarToken> tokens) throws GrammarParseException {
        return new GrammarParser(tokens, null).parse();
    }

    /**
     * Parse the given list of tokens.
     * <p>
     * This version of <pre>parse()</pre> accepts the original text to be parsed as a param. This is purely to be used
     * to in error handling to calculate the error position as a line number + column number. If the original text is 
     * not provided then the character position in the stream is displayed instead.
     *
     * @param tokens the tokens emitted by the Lexer
     * @param text the original text that was tokenised - this is only used for error messaging              
     * @return the abstract syntax tree as a result of the parse
     * @see #parse(List)
     */
    public static ProductionRules parse(List<GrammarToken> tokens, CharSequence text) throws GrammarParseException {
        return new GrammarParser(tokens, text).parse();
    }

    private ProductionRules parse() throws GrammarParseException {
        ProductionRules result = parseProductionRules();
        if (result != null && tokenStream.isEof()) {
            return result;
        } else {
            throw new GrammarParseException(tokenStream.getCharIndex(failRegister.depth), failRegister.expectedTokens, text);
        }
    }

    private GrammarParser(List<GrammarToken> tokens, CharSequence text) {
        this.tokenStream = new GrammarTokenStream(tokens);
        this.text = text;
    }

    private ProductionRules parseProductionRules() {
        return parseProductionRules_v0();
    }

    private ProductionRules parseProductionRules_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseProductionRule();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        }
        while (r0 != null) {
            childNodes.add(r0);
            r0 = parseProductionRule();
        }

       return new ProductionRules(childNodes);
    }

    private ProductionRule parseProductionRule() {
        return parseProductionRule_v0();
    }

    private ProductionRule parseProductionRule_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRuleName();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseColon();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseCompoundRule();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }
        AstNode r3 = parseSemicolon();
        if (r3 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r3);
        }

       return new ProductionRule(childNodes);
    }

    private CompoundRule parseCompoundRule() {
        return parseCompoundRule_v0();
    }

    private CompoundRule parseCompoundRule_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRuleSequence();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        List<AstNode> r1 = parseCompoundRule_v0_r1();
        while (r1 != null) {
            childNodes.addAll(r1);
            r1 = parseCompoundRule_v0_r1();
        }

       return new CompoundRule(childNodes);
    }

    private List<AstNode> parseCompoundRule_v0_r1() {
        return parseCompoundRule_v0_r1_v0();
    }

    private List<AstNode> parseCompoundRule_v0_r1_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parsePipe();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseRuleSequence();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }

       return childNodes;
    }

    private RuleSequence parseRuleSequence() {
        return parseRuleSequence_v0();
    }

    private RuleSequence parseRuleSequence_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseQuantifiedRule();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        }
        while (r0 != null) {
            childNodes.add(r0);
            r0 = parseQuantifiedRule();
        }

       return new RuleSequence(childNodes);
    }

    private QuantifiedRule parseQuantifiedRule() {
        return parseQuantifiedRule_v0();
    }

    private QuantifiedRule parseQuantifiedRule_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseSingleRule();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseQuantifier();
        if (r1 != null) {
            childNodes.add(r1);
        }

       return new QuantifiedRule(childNodes);
    }

    private Quantifier parseQuantifier() {
        Quantifier result;
        result = parseQuantifier_v0();
        if (result != null) return result;
        result = parseQuantifier_v1();
        if (result != null) return result;
        return parseQuantifier_v2();
    }

    private Quantifier parseQuantifier_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parsePlus();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new Quantifier(childNodes);
    }

    private Quantifier parseQuantifier_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseStar();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new Quantifier(childNodes);
    }

    private Quantifier parseQuantifier_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseQuestionMark();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new Quantifier(childNodes);
    }

    private SingleRule parseSingleRule() {
        SingleRule result;
        result = parseSingleRule_v0();
        if (result != null) return result;
        result = parseSingleRule_v1();
        if (result != null) return result;
        return parseSingleRule_v2();
    }

    private SingleRule parseSingleRule_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseTokenName();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new SingleRule(childNodes);
    }

    private SingleRule parseSingleRule_v1() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseRuleName();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new SingleRule(childNodes);
    }

    private SingleRule parseSingleRule_v2() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseNestedRule();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }

       return new SingleRule(childNodes);
    }

    private NestedRule parseNestedRule() {
        return parseNestedRule_v0();
    }

    private NestedRule parseNestedRule_v0() {
        List<AstNode> childNodes = new LinkedList<>();
        GrammarTokenStream.Marker marker = tokenStream.mark();

        AstNode r0 = parseLParen();
        if (r0 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r0);
        }
        AstNode r1 = parseCompoundRule();
        if (r1 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r1);
        }
        AstNode r2 = parseRParen();
        if (r2 == null) {
            tokenStream.rollback(marker);
            return null;
        } else {
            childNodes.add(r2);
        }

       return new NestedRule(childNodes);
    }
    private QuestionMark parseQuestionMark() {
        if (tokenStream.tokenType() == GrammarToken.Type.QUESTION_MARK) {
            return new QuestionMark(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.QUESTION_MARK);
            return null;
        }
    }
    private Star parseStar() {
        if (tokenStream.tokenType() == GrammarToken.Type.STAR) {
            return new Star(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.STAR);
            return null;
        }
    }
    private Semicolon parseSemicolon() {
        if (tokenStream.tokenType() == GrammarToken.Type.SEMICOLON) {
            return new Semicolon(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.SEMICOLON);
            return null;
        }
    }
    private TokenName parseTokenName() {
        if (tokenStream.tokenType() == GrammarToken.Type.TOKEN_NAME) {
            return new TokenName(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.TOKEN_NAME);
            return null;
        }
    }
    private RuleName parseRuleName() {
        if (tokenStream.tokenType() == GrammarToken.Type.RULE_NAME) {
            return new RuleName(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.RULE_NAME);
            return null;
        }
    }
    private Colon parseColon() {
        if (tokenStream.tokenType() == GrammarToken.Type.COLON) {
            return new Colon(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.COLON);
            return null;
        }
    }
    private Pipe parsePipe() {
        if (tokenStream.tokenType() == GrammarToken.Type.PIPE) {
            return new Pipe(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.PIPE);
            return null;
        }
    }
    private LParen parseLParen() {
        if (tokenStream.tokenType() == GrammarToken.Type.L_PAREN) {
            return new LParen(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.L_PAREN);
            return null;
        }
    }
    private RParen parseRParen() {
        if (tokenStream.tokenType() == GrammarToken.Type.R_PAREN) {
            return new RParen(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.R_PAREN);
            return null;
        }
    }
    private Plus parsePlus() {
        if (tokenStream.tokenType() == GrammarToken.Type.PLUS) {
            return new Plus(tokenStream.next().getText());
        } else {
            failRegister.registerFail(tokenStream.getIndex(), GrammarToken.Type.PLUS);
            return null;
        }
    }

    private static final class FailRegister {
        private int depth = -1;
        private final HashSet<GrammarToken.Type> expectedTokens = new HashSet<>();

        public void registerFail(int index, GrammarToken.Type expectedToken) {
            if (index == depth) {
                expectedTokens.add(expectedToken);
            } else if (index > depth) {
                depth = index;
                expectedTokens.clear();
                expectedTokens.add(expectedToken);
            }
        }
    }
}    
