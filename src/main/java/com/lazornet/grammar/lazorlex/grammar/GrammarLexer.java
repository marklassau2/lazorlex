package com.lazornet.grammar.lazorlex.grammar;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class GrammarLexer {
    private static final TokenMatcher matcher0 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher1 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch == '#') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (!(ch == '\r' || ch == '\n' || ch == (char) 0)) {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher2 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch >= 'a' && ch <= 'z') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_' || ch >= '0' && ch <= '9') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher3 = charStream -> {
        char ch;
        int count = 0;
        ch = charStream.read();
        if (ch >= 'A' && ch <= 'Z') {
            count++;
            ch = charStream.read();
        } else {
            return -1;
        }
        while (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch == '_' || ch >= '0' && ch <= '9') {
            count++;
            ch = charStream.read();
        }
        return count;
    };
    private static final TokenMatcher matcher4 = charStream -> {
        // Exact text ':'
        char ch;
        ch = charStream.read();
        if (ch != ':') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher5 = charStream -> {
        // Exact text ';'
        char ch;
        ch = charStream.read();
        if (ch != ';') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher6 = charStream -> {
        // Exact text '|'
        char ch;
        ch = charStream.read();
        if (ch != '|') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher7 = charStream -> {
        // Exact text '('
        char ch;
        ch = charStream.read();
        if (ch != '(') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher8 = charStream -> {
        // Exact text ')'
        char ch;
        ch = charStream.read();
        if (ch != ')') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher9 = charStream -> {
        // Exact text '*'
        char ch;
        ch = charStream.read();
        if (ch != '*') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher10 = charStream -> {
        // Exact text '+'
        char ch;
        ch = charStream.read();
        if (ch != '+') {
            return -1;
        }
        return 1;
    };
    private static final TokenMatcher matcher11 = charStream -> {
        // Exact text '?'
        char ch;
        ch = charStream.read();
        if (ch != '?') {
            return -1;
        }
        return 1;
    };

    private static final LexerState[] states = {
            new LexerState("default", asList(
                    new LexerRule(matcher0, GrammarToken.Type._WHITESPACE, -1),
                    new LexerRule(matcher1, GrammarToken.Type._COMMENT, -1),
                    new LexerRule(matcher2, GrammarToken.Type.RULE_NAME, -1),
                    new LexerRule(matcher3, GrammarToken.Type.TOKEN_NAME, -1),
                    new LexerRule(matcher4, GrammarToken.Type.COLON, -1),
                    new LexerRule(matcher5, GrammarToken.Type.SEMICOLON, -1),
                    new LexerRule(matcher6, GrammarToken.Type.PIPE, -1),
                    new LexerRule(matcher7, GrammarToken.Type.L_PAREN, -1),
                    new LexerRule(matcher8, GrammarToken.Type.R_PAREN, -1),
                    new LexerRule(matcher9, GrammarToken.Type.STAR, -1),
                    new LexerRule(matcher10, GrammarToken.Type.PLUS, -1),
                    new LexerRule(matcher11, GrammarToken.Type.QUESTION_MARK, -1)
            )),
    };

    private LexingCharStream charStream;
    private int state = 0;
    private GrammarToken.Type tokenType;
    private int tokenEnd = -1;

    public static List<GrammarToken> lexAllTokens(CharSequence text) {
        final List<GrammarToken> tokens = new LinkedList<>();
        GrammarLexer lexer = new GrammarLexer();
        lexer.start(text);
        while (lexer.getTokenType() != null) {
            if (!lexer.getTokenType().name().startsWith("_")) {
                tokens.add(new GrammarToken(lexer.getTokenType(), lexer.getTokenStart(), lexer.getTokenText()));
            }
            lexer.advance();
        }
        return tokens;
    }

    public void start(CharSequence buffer) {
        reset(buffer, 0, 0);
    }

    public void reset(CharSequence buffer, int startOffset, int initialState) {
        this.charStream = new LexingCharStream(buffer, startOffset);
        this.state = initialState;
        this.tokenEnd = startOffset;

        readToken();
    }

    public void readToken() {
        if (tokenEnd >= charStream.length()) {
            tokenType = null;
            return;
        }

        int bestLength = 0;
        LexerRule bestMatch = null;
        for (LexerRule rule : states[state].getRules()) {
            charStream.rewind();
            int matchLength = rule.match(charStream);
            if (matchLength > bestLength) {
                bestLength = matchLength;
                bestMatch = rule;
            }
        }
        if (bestMatch == null) {
            throw new IllegalStateException("syntax error: no matching token found at " +
                    charStream.getTokenStart() + " (state: " + states[state].name + ")\n");
        }
        if (bestMatch.hasNextState()) {
            state = bestMatch.nextState;
        }
        tokenEnd = charStream.getTokenStart() + bestLength;
        tokenType = bestMatch.tokenType;
    }

    public void advance() {
        charStream.setTokenStart(tokenEnd);
        readToken();
    }

    public int getState() {
        return state;
    }

    public int getTokenEnd() {
        return tokenEnd;
    }

    public GrammarToken.Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return charStream.getTokenStart();
    }

    public String getTokenText() {
        return charStream.getTextBuffer().subSequence(getTokenStart(), tokenEnd).toString();
    }

    private static class LexerState {
        private final String name;
        private final Iterable<LexerRule> rules;

        private LexerState(String name, Iterable<LexerRule> rules) {
            this.name = name;
            this.rules = rules;
        }

        private Iterable<LexerRule> getRules() {
            return rules;
        }
    }

    private static class LexerRule {
        private final TokenMatcher tokenMatcher;
        private final int nextState;
        private final GrammarToken.Type tokenType;

        private LexerRule(TokenMatcher tokenMatcher, GrammarToken.Type tokenType, int nextState) {
            this.tokenMatcher = tokenMatcher;
            this.tokenType = tokenType;
            this.nextState = nextState;
        }

        private int match(LexingCharStream charStream) {
            return tokenMatcher.match(charStream);
        }

        private boolean hasNextState() {
            return nextState >= 0;
        }
    }

    interface TokenMatcher {
        int match(LexingCharStream charStream);
    }
}
