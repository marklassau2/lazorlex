package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the COLON token.
 */
public class Colon extends AstNode.TokenNode {

    public Colon(String text) {
        super(text);
    }
}
