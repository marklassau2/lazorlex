package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the RULE_NAME token.
 */
public class RuleName extends AstNode.TokenNode {

    public RuleName(String text) {
        super(text);
    }
}
