package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the SEMICOLON token.
 */
public class Semicolon extends AstNode.TokenNode {

    public Semicolon(String text) {
        super(text);
    }
}
