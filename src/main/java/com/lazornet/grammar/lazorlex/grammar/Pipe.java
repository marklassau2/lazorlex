package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the PIPE token.
 */
public class Pipe extends AstNode.TokenNode {

    public Pipe(String text) {
        super(text);
    }
}
