package com.lazornet.grammar.lazorlex.grammar;

/**
 * This node represents the L_PAREN token.
 */
public class LParen extends AstNode.TokenNode {

    public LParen(String text) {
        super(text);
    }
}
