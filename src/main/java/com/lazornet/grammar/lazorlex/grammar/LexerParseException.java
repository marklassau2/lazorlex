package com.lazornet.grammar.lazorlex.grammar;

public class LexerParseException extends RuntimeException {
    public LexerParseException(String message) {
        super(message);
    }
}
