package com.lazornet.grammar.lazorlex.grammar;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Indicates an error during parsing.
 * <p>
 * The {@link #getMessage()} method will return a human-readable message indicating the error.
 * <p>
 * If you want to display the error yourself (eg in a text editor) then use {@link #getCharIndex()} and {@link #getExpectedTokens()}.
 */
public class GrammarParseException extends Exception {
    private final int charIndex;
    private final Set<GrammarToken.Type> expectedTokens;
    private final CharSequence text;

    public GrammarParseException(int charIndex, Set<GrammarToken.Type> expectedTokens, CharSequence text) {
        this.charIndex = charIndex;
        this.expectedTokens = expectedTokens;
        this.text = text;
    }

    @Override
    public String getMessage() {
        String message = "Parse failed at " + getPosition();
        if (expectedTokens.size() == 1) {
            message += ". Expected " + expectedTokens.iterator().next();
        } else {
            message += ". Expected one of the following: "
                    + expectedTokens.stream()
                    .map(t -> t.name())
                    .collect(Collectors.joining(", "));
        }
        return message;
    }

    /**
     * Returns the index in the original text where the parse error occurred.
     * Note that this is 0-indexed.
     * 
     * @return the index in the original text where the parse error occurred.
     */
    public int getCharIndex() {
        return charIndex;
    }

    /**
     * The parser expected to see one of the given tokens at the error position.
     * 
     * @return the set of tokens we expected to see at the error position.
     */
    public Set<GrammarToken.Type> getExpectedTokens() {
        return expectedTokens;
    }

    /**
     * Returns a string indicating the position of the error.
     * The format depends on whether you supplied the original text to the parser, or just the list of tokens.
     * If the original text was supplied, the String will show the line-number and column-number where the error 
     * occurred eg "3:13".
     * This format is intended for human readability and is therefore 1-indexed.
     * If the original text was not supplied, then it returns the 0-indexed character index.
     * 
     * @return a representation of where the error occurred in the parsed text.
     */
    public String getPosition() {
        if (text == null) return "" + charIndex;
        int lineNumber = 1;
        int columnNumber = 1;
        int idx = 0;
        while (idx < charIndex) {
            switch (text.charAt(idx)) {
                case '\n':
                    lineNumber++;
                    columnNumber = 0;
                    break;
                case '\r':
                    lineNumber++;
                    columnNumber = 0;
                    if (idx + 1 < charIndex && text.charAt(idx + 1) == '\n') {
                        // CRLF - nibble one more char
                        idx++;
                    }
                    break;
                default:
                    columnNumber++;
                    break;
            }
            idx++;
        }
        columnNumber++;
        return "" + lineNumber + ':'+ columnNumber;
    }
}
