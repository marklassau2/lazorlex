package com.lazornet.grammar.lex.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClassNameUtilTest {

    @Test
    public void test_classNameFor() {
        assertEquals("Foo", ClassNameUtil.classNameFor("FOO"));
        assertEquals("FooBar", ClassNameUtil.classNameFor("FOO_BAR"));
        assertEquals("WhereTheWildThingsAre", ClassNameUtil.classNameFor("WHERE_THE_WILD_THINGS_ARE"));

        assertEquals("Cat", ClassNameUtil.classNameFor("cat"));
        assertEquals("CatFish", ClassNameUtil.classNameFor("catFish"));
        assertEquals("RedFishBlueFish", ClassNameUtil.classNameFor("redFishBlueFish"));
        assertEquals("XMLParser", ClassNameUtil.classNameFor("xMLParser"));

    }
}
