package com.lazornet.grammar.lex.parsergenerator;

import com.lazornet.grammar.lazorlex.grammar.*;
import org.junit.Test;

import java.util.List;

import static com.lazornet.grammar.lex.parsergenerator.QuantifiedRule.Quantifier.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class GrammarCompilerTest {

    @Test
    public void simpleTest() throws GrammarParseException {
        List<GrammarToken> tokens = GrammarLexer.lexAllTokens("" +
                "animals: animal+ ;\n" +
                "animal: CAT | DOG;\n" +
                "");
        ProductionRules productionRules = GrammarParser.parse(tokens);
        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        assertEquals(asList("CAT", "DOG"), grammarDefinition.getTokenNames());

        NamedProductionRule animalsRule = new NamedProductionRule("animals",
                singletonList(new RuleVariant(singletonList(new QuantifiedRule("animal", null, ONE_OR_MORE)))),
                CardinalityMap.ofOne("animal").many());
        NamedProductionRule animalRule = new NamedProductionRule("animal",
                asList(
                        new RuleVariant(singletonList(new QuantifiedRule("CAT", null, ONE))),
                        new RuleVariant(singletonList(new QuantifiedRule("DOG", null, ONE)))
                ),
                CardinalityMap.ofOne("CAT").or(CardinalityMap.ofOne("DOG")));

        assertEquals(asList(animalsRule, animalRule), grammarDefinition.getNamedRules());
    }

    @Test
    public void testQuantifiers() throws GrammarParseException {
        List<GrammarToken> tokens = GrammarLexer.lexAllTokens("" +
                "animals: animal+ ;\n" +
                "animal: BLACK? FERAL* DOG;" +
                "");
        ProductionRules productionRules = GrammarParser.parse(tokens);
        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        assertEquals(asList("FERAL", "BLACK", "DOG"), grammarDefinition.getTokenNames());

        NamedProductionRule animalsRule = new NamedProductionRule("animals",
                singletonList(new RuleVariant(singletonList(new QuantifiedRule("animal", null, ONE_OR_MORE)))),
                CardinalityMap.ofOne("animal").many());
        NamedProductionRule animalRule = new NamedProductionRule("animal",
                singletonList(
                        new RuleVariant(asList(
                                new QuantifiedRule("BLACK", null, MAYBE_ONE),
                                new QuantifiedRule("FERAL", null, ZERO_OR_MORE),
                                new QuantifiedRule("DOG", null, ONE)
                        ))
                ),
                CardinalityMap.ofOne("BLACK").maybeOne()
                        .plus(CardinalityMap.ofOne("FERAL").many())
                        .plus(CardinalityMap.ofOne("DOG"))
        );

        assertEquals(asList(animalsRule, animalRule), grammarDefinition.getNamedRules());
    }

    @Test
    public void testNestedVariants1() throws GrammarParseException {
        List<GrammarToken> tokens = GrammarLexer.lexAllTokens("" +
                "animals: animal+ ;\n" +
                "animal: BLACK? (CAT | DOG);" +
                "");
        ProductionRules productionRules = GrammarParser.parse(tokens);
        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        assertEquals(asList("CAT", "BLACK", "DOG"), grammarDefinition.getTokenNames());

        NamedProductionRule animalsRule = new NamedProductionRule("animals",
                singletonList(new RuleVariant(singletonList(new QuantifiedRule("animal", null, ONE_OR_MORE)))),
                CardinalityMap.ofOne("animal").many());
        List<RuleVariant> nestedVariants = asList(
                new RuleVariant(singletonList(new QuantifiedRule("CAT", null, ONE))),
                new RuleVariant(singletonList(new QuantifiedRule("DOG", null, ONE)))
        );
        NamedProductionRule animalRule = new NamedProductionRule("animal",
                singletonList(
                        new RuleVariant(asList(
                                new QuantifiedRule("BLACK", null, MAYBE_ONE),
                                new QuantifiedRule(null, nestedVariants, ONE)
                        ))
                ),
                CardinalityMap.ofOne("BLACK").maybeOne()
                        .plus(CardinalityMap.ofOne("CAT").maybeOne())
                        .plus(CardinalityMap.ofOne("DOG").maybeOne())
        );

        assertEquals(asList(animalsRule, animalRule), grammarDefinition.getNamedRules());
    }

    @Test
    public void testNestedVariants2() throws GrammarParseException {
        List<GrammarToken> tokens = GrammarLexer.lexAllTokens("" +
                "animals: animal+ ;\n" +
                "animal: BLACK? (CAT | CAT DOG);" +
                "");
        ProductionRules productionRules = GrammarParser.parse(tokens);
        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        assertEquals(asList("CAT", "BLACK", "DOG"), grammarDefinition.getTokenNames());

        NamedProductionRule animalsRule = new NamedProductionRule("animals",
                singletonList(new RuleVariant(singletonList(new QuantifiedRule("animal", null, ONE_OR_MORE)))),
                CardinalityMap.ofOne("animal").many());
        List<RuleVariant> nestedVariants = asList(
                new RuleVariant(singletonList(new QuantifiedRule("CAT", null, ONE))),
                new RuleVariant(asList(
                        new QuantifiedRule("CAT", null, ONE),
                        new QuantifiedRule("DOG", null, ONE)
                ))
        );
        NamedProductionRule animalRule = new NamedProductionRule("animal",
                singletonList(
                        new RuleVariant(asList(
                                new QuantifiedRule("BLACK", null, MAYBE_ONE),
                                new QuantifiedRule(null, nestedVariants, ONE)
                        ))
                ),
                CardinalityMap.ofOne("BLACK").maybeOne()
                        .plus(CardinalityMap.ofOne("CAT"))
                        .plus(CardinalityMap.ofOne("DOG").maybeOne())
        );

        assertEquals(asList(animalsRule, animalRule), grammarDefinition.getNamedRules());
    }

    @Test
    public void testNestedVariants3() throws GrammarParseException {
        List<GrammarToken> tokens = GrammarLexer.lexAllTokens("" +
                "animals: animal+ ;\n" +
                "animal: BLACK? (CAT | DOG)*;" +
                "");
        ProductionRules productionRules = GrammarParser.parse(tokens);
        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(productionRules);

        assertEquals(asList("CAT", "BLACK", "DOG"), grammarDefinition.getTokenNames());

        NamedProductionRule animalsRule = new NamedProductionRule("animals",
                singletonList(new RuleVariant(singletonList(new QuantifiedRule("animal", null, ONE_OR_MORE)))),
                CardinalityMap.ofOne("animal").many());
        List<RuleVariant> nestedVariants = asList(
                new RuleVariant(singletonList(new QuantifiedRule("CAT", null, ONE))),
                new RuleVariant(singletonList(new QuantifiedRule("DOG", null, ONE)))
        );
        NamedProductionRule animalRule = new NamedProductionRule("animal",
                singletonList(
                        new RuleVariant(asList(
                                new QuantifiedRule("BLACK", null, MAYBE_ONE),
                                new QuantifiedRule(null, nestedVariants, ZERO_OR_MORE)
                        ))
                ),
                CardinalityMap.ofOne("BLACK").maybeOne()
                        .plus(CardinalityMap.ofOne("CAT").many())
                        .plus(CardinalityMap.ofOne("DOG").many())
        );

        assertEquals(asList(animalsRule, animalRule), grammarDefinition.getNamedRules());
    }
}
