package com.lazornet.grammar.lex.parsergenerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CardinalityMapTest {
    @Test
    public void test() {
        CardinalityMap cmap = CardinalityMap.ofOne("a")
                .plus(CardinalityMap.ofOne("b").many());
        assertEquals(Cardinality.ONE, cmap.get("a"));
        assertEquals(Cardinality.MANY, cmap.get("b"));

        cmap = cmap.maybeOne();
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("a"));
        assertEquals(Cardinality.MANY, cmap.get("b"));

        cmap = cmap.plus(CardinalityMap.ofOne("a").maybeOne());
        assertEquals(Cardinality.MANY, cmap.get("a"));
        assertEquals(Cardinality.MANY, cmap.get("b"));
    }

    @Test
    public void testPlus() {
        CardinalityMap cmap;

        cmap = CardinalityMap.ofOne("foo")
                .plus(CardinalityMap.ofOne("bar"))
        ;
        assertEquals(Cardinality.ONE, cmap.get("foo"));
        assertEquals(Cardinality.ONE, cmap.get("bar"));

        cmap = CardinalityMap.ofOne("a")
                .plus(CardinalityMap.ofOne("a"))
                .plus(CardinalityMap.ofOne("b").maybeOne())
                .plus(CardinalityMap.ofOne("b"))
                .plus(CardinalityMap.ofOne("c").maybeOne())
                .plus(CardinalityMap.ofOne("c").maybeOne())
                .plus(CardinalityMap.ofOne("d").many())
                .plus(CardinalityMap.ofOne("d").maybeOne())
                .plus(CardinalityMap.ofOne("t"))
                .plus(CardinalityMap.ofOne("u").maybeOne())
                .plus(CardinalityMap.ofOne("v").many())
        ;
        assertEquals(Cardinality.MANY, cmap.get("a"));
        assertEquals(Cardinality.MANY, cmap.get("b"));
        assertEquals(Cardinality.MANY, cmap.get("c"));
        assertEquals(Cardinality.MANY, cmap.get("d"));
        assertEquals(Cardinality.ONE, cmap.get("t"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("u"));
        assertEquals(Cardinality.MANY, cmap.get("v"));
    }

    @SuppressWarnings("SimplifiableJUnitAssertion")
    @Test
    public void testOr() {
        CardinalityMap cmap1 = CardinalityMap.ofOne("a1")
                .plus(CardinalityMap.ofOne("a2"))
                .plus(CardinalityMap.ofOne("a3"))
                .plus(CardinalityMap.ofOne("a4"))
                .plus(CardinalityMap.ofOne("b1").maybeOne())
                .plus(CardinalityMap.ofOne("b2").maybeOne())
                .plus(CardinalityMap.ofOne("b3").maybeOne())
                .plus(CardinalityMap.ofOne("b4").maybeOne())
                .plus(CardinalityMap.ofOne("c").many());
        assertEquals(Cardinality.ONE, cmap1.get("a1"));
        assertEquals(Cardinality.ONE, cmap1.get("a2"));
        assertEquals(Cardinality.ONE, cmap1.get("a3"));
        assertEquals(Cardinality.ONE, cmap1.get("a4"));
        assertEquals(Cardinality.MAYBE_ONE, cmap1.get("b1"));
        assertEquals(Cardinality.MAYBE_ONE, cmap1.get("b2"));
        assertEquals(Cardinality.MAYBE_ONE, cmap1.get("b3"));
        assertEquals(Cardinality.MAYBE_ONE, cmap1.get("b4"));
        assertEquals(null, cmap1.get("b5"));
        assertEquals(Cardinality.MANY, cmap1.get("c"));

        CardinalityMap cmap2 = CardinalityMap.ofOne("a1")
                .plus(CardinalityMap.ofOne("a2").maybeOne())
                .plus(CardinalityMap.ofOne("a3").many())
                .plus(CardinalityMap.ofOne("a5"))
                .plus(CardinalityMap.ofOne("b1"))
                .plus(CardinalityMap.ofOne("b2").maybeOne())
                .plus(CardinalityMap.ofOne("b3").many())
                .plus(CardinalityMap.ofOne("b5").maybeOne())
                .plus(CardinalityMap.ofOne("c").many());
        assertEquals(Cardinality.ONE, cmap2.get("a1"));
        assertEquals(Cardinality.MAYBE_ONE, cmap2.get("a2"));
        assertEquals(Cardinality.MANY, cmap2.get("a3"));
        assertNull(cmap2.get("a4"));
        assertEquals(Cardinality.ONE, cmap2.get("a5"));
        assertEquals(Cardinality.ONE, cmap2.get("b1"));
        assertEquals(Cardinality.MAYBE_ONE, cmap2.get("b2"));
        assertEquals(Cardinality.MANY, cmap2.get("b3"));
        assertEquals(null, cmap2.get("b4"));
        assertEquals(Cardinality.MAYBE_ONE, cmap2.get("b5"));
        assertEquals(Cardinality.MANY, cmap2.get("c"));

        CardinalityMap cmap = cmap1.or(cmap2);
        assertEquals(Cardinality.ONE, cmap.get("a1"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("a2"));
        assertEquals(Cardinality.MANY, cmap.get("a3"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("a4"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("a5"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("b1"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("b2"));
        assertEquals(Cardinality.MANY, cmap.get("b3"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("b4"));
        assertEquals(Cardinality.MAYBE_ONE, cmap.get("b5"));
    }

}
