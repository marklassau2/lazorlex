package com.lazornet.grammar.lex.parsergenerator;

import com.lazornet.grammar.lazorlex.grammar.QuantifiedRule;
import com.lazornet.grammar.lazorlex.grammar.*;
import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class CardinalityAnalyserTest {
    @Test
    public void testSimple() {
        AstNode quantifiedRule1 = new QuantifiedRule(singletonList(
                new SingleRule(singletonList(new RuleName("foo")))
        ));
        AstNode quantifiedRule2 = new QuantifiedRule(asList(
                new SingleRule(singletonList(new RuleName("bar"))),
                new Quantifier(singletonList(new QuestionMark("?")))
        ));
        AstNode quantifiedRule3 = new QuantifiedRule(asList(
                new SingleRule(singletonList(new RuleName("cat"))),
                new Quantifier(singletonList(new Plus("+")))
        ));
        AstNode quantifiedRule4 = new QuantifiedRule(asList(
                new SingleRule(singletonList(new RuleName("dog"))),
                new Quantifier(singletonList(new Star("*")))
        ));
        AstNode ruleSequence1 = new RuleSequence(asList(
                quantifiedRule1,
                quantifiedRule2,
                quantifiedRule3,
                quantifiedRule4
        ));
        CompoundRule compoundRule = new CompoundRule(singletonList(ruleSequence1));

        CardinalityMap cardinalityMap = CardinalityAnalyser.analyseCardinality(compoundRule);
        assertEquals(4, cardinalityMap.entries().size());
        assertEquals(Cardinality.ONE, cardinalityMap.get("foo"));
        assertEquals(Cardinality.MAYBE_ONE, cardinalityMap.get("bar"));
        assertEquals(Cardinality.MANY, cardinalityMap.get("cat"));
        assertEquals(Cardinality.MANY, cardinalityMap.get("dog"));
    }
}
