package com.lazornet.grammar.lex.parsergenerator;

import org.junit.Test;

import java.io.PrintWriter;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class ParserGeneratorTest {
    @Test
    public void test_tokenClassName() throws Exception {
        assertEquals("FooBar", ParserGenerator.tokenClassName("FOO_BAR"));
    }

    @Test
    public void test() {
        String packageName = "com.foo";
        String strippedFileName = "Foo";
        List<String> tokenNames = asList("DING_DONG", "FOO");
        QuantifiedRule batsRule = new QuantifiedRule(null,
                asList(new RuleVariant(asList(new QuantifiedRule("BAT_MAN", null, QuantifiedRule.Quantifier.ONE_OR_MORE)))),
                QuantifiedRule.Quantifier.ONE);
        List<RuleVariant> variants = asList(new RuleVariant(asList(batsRule)));
        CardinalityMap cardinalityMap = CardinalityMap.ofOne("FOO");
        List<NamedProductionRule> namedRules = asList(
                new NamedProductionRule("Animal", variants, cardinalityMap)
        );
        GrammarDefinition grammarDefinition = new GrammarDefinition(namedRules, tokenNames);


//        List<AstNode> nodes = Arrays.asList(
//                new Animal
//        );
//        NamedRules namedRules = new NamedRules(nodes);
//        GrammarDefinition grammarDefinition = GrammarCompiler.compileGrammarDefinition(namedRules);

        PrintWriter out = new PrintWriter(System.out);
        new ParserClassGenerator(packageName, strippedFileName, grammarDefinition).generate(out);
        out.flush();
    }
}
