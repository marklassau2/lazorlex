package com.lazornet.grammar.lex.lexergenerator;

import com.lazornet.grammar.lazorlex.lex.*;
import com.lazornet.grammar.lex.ResourceReader;
import com.lazornet.grammar.lex.lexercompiler.LexerCompiler;
import com.lazornet.grammar.lex.lexercompiler.LexerDefinition;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.List;

public class LexerClassGeneratorTest {
    @Test
    public void test() throws LexParseException {
        List<LexToken> tokens = LexLexer.lexAllTokens(
                "init {\n" +
                        "    TOKEN_NAME      /[A-Z_][A-Z_0-9]*/\n" +
                        "    STATE_NAME      /[a-z][a-z_0-9]*/\n" +
                        "    MACRO_NAME      /$[a-zA-Z][a-zA-Z_0-9]*/\n" +
                        "    L_BRACE         \"{\"\n" +
                        "    R_BRACE         \"}\"\n" +
                        "    SLASH           \"/\"         -> regex\n" +
                        "    DOUBLE_QUOTE    /\"/         -> exact_text\n" +
                        "    R_ARROW         \"->\"\n" +
                        "    _WHITESPACE     /[ \\t\\r\\n]+/\n" +
                        "}\n" +
                        "\n" +
                        "exact_text {\n" +
                        "    DOUBLE_QUOTE    /\"/         -> init\n" +
                        "    TEXT            /~\"+/\n" +
                        "}\n" +
                        "\n" +
                        "regex {\n" +
                        "    SLASH           \"/\"         -> init\n" +
                        "    L_BRACKET       \"[\"         -> regex_alt\n" +
                        "    TILDE           \"~\"\n" +
                        "    DOT             \".\"\n" +
                        "    STAR            \"*\"\n" +
                        "    PLUS            \"+\"\n" +
                        "    ESC_BACKSLASH   \"\\\\\"\n" +
                        "    CHAR            /./\n" +
                        "}\n" +
                        "\n" +
                        "regex_alt {\n" +
                        "    R_BRACKET       \"]\"         -> regex\n" +
                        "    DASH            \"-\"\n" +
                        "    ESC_BACKSLASH   \"\\\\\"\n" +
                        "    ESC_TAB         \"\\t\"\n" +
                        "    ESC_CR          \"\\r\"\n" +
                        "    ESC_LF          \"\\n\"\n" +
                        "    CHAR            /./\n" +
                        "}\n");

        Statements statements = LexParser.parse(tokens);
        LexerDefinition lexerDefinition = LexerCompiler.compile(statements);
        LexerClassGenerator lexerClassGenerator = new LexerClassGenerator("com.lazornet.grammar.lex.lexerparser", "Foo", lexerDefinition);
        StringWriter out = new StringWriter();
        lexerClassGenerator.generate(new PrintWriter(out));

        String expected = ResourceReader.readResourceFile("ExpectedLexer.txt");
        Assert.assertEquals(expected, out.toString());
    }
}
