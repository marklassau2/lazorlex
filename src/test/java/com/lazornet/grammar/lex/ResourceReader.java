package com.lazornet.grammar.lex;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ResourceReader {
    public static String readResourceFile(String name) {
        final InputStream inputStream = ResourceReader.class.getClassLoader().getResourceAsStream(name);
        if (inputStream == null) throw new IllegalArgumentException("Resource file not found: " + name);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(buffer)) != -1) {
                out.write(buffer, 0, length);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        return out.toString();
    }
}
