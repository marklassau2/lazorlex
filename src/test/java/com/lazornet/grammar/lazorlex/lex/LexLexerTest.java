package com.lazornet.grammar.lazorlex.lex;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static com.lazornet.grammar.lazorlex.lex.LexToken.Type.*;

public class LexLexerTest {

    @Test
    public void testSimple() {
        List<LexToken> tokens = LexLexer.lexAllTokens(
                "init {\n" +
                        "    TOKEN_NAME      /[A-Z_][A-Z_0-9]*/\n" +
                        "    L_BRACE         \"{\"      -> foo_state\n" +
                        "}\n" +
                        "\n" +
                        "foo_state {\n" +
                        "    SLASH           \"/\"\n" +
                        "    STAR            \"*\"\n" +
                        "    PLUS            \"+\"\n" +
                        "    R_BRACE         \"}\"         -> init\n" +
                        "}\n");

        List<LexToken> expected = Arrays.asList(
                token(STATE_NAME, "init"),
                token(L_BRACE, "{"),
                token(TOKEN_NAME, "TOKEN_NAME"),
                token(SLASH, "/"),
                token(L_BRACKET, "["),
                token(CHAR, "A"),
                token(DASH, "-"),
                token(CHAR, "Z"),
                token(CHAR, "_"),
                token(R_BRACKET, "]"),
                token(L_BRACKET, "["),
                token(CHAR, "A"),
                token(DASH, "-"),
                token(CHAR, "Z"),
                token(CHAR, "_"),
                token(CHAR, "0"),
                token(DASH, "-"),
                token(CHAR, "9"),
                token(R_BRACKET, "]"),
                token(STAR, "*"),
                token(SLASH, "/"),
                token(TOKEN_NAME, "L_BRACE"),
                token(DOUBLE_QUOTE, "\""),
                token(TEXT, "{"),
                token(DOUBLE_QUOTE, "\""),
                token(R_ARROW, "->"),
                token(STATE_NAME, "foo_state"),
                token(R_BRACE, "}"),
                token(STATE_NAME, "foo_state"),
                token(L_BRACE, "{"),
                token(TOKEN_NAME, "SLASH"),
                token(DOUBLE_QUOTE, "\""),
                token(TEXT, "/"),
                token(DOUBLE_QUOTE, "\""),
                token(TOKEN_NAME, "STAR"),
                token(DOUBLE_QUOTE, "\""),
                token(TEXT, "*"),
                token(DOUBLE_QUOTE, "\""),
                token(TOKEN_NAME, "PLUS"),
                token(DOUBLE_QUOTE, "\""),
                token(TEXT, "+"),
                token(DOUBLE_QUOTE, "\""),
                token(TOKEN_NAME, "R_BRACE"),
                token(DOUBLE_QUOTE, "\""),
                token(TEXT, "}"),
                token(DOUBLE_QUOTE, "\""),
                token(R_ARROW, "->"),
                token(STATE_NAME, "init"),
                token(R_BRACE, "}")

        );
        assertEqualTokens(expected, tokens);
    }

    @Test
    public void testComment() {
        List<LexToken> tokens = LexLexer.lexAllTokens(
                "# Blah Blah {\n" +
                        "init {  # initial state\n" +
                        "    IDENT      /[A-Z]+/ # TODO: 0-9 and _\n" +
                        "}\n");

        List<LexToken> expected = Arrays.asList(
                token(STATE_NAME, "init"),
                token(L_BRACE, "{"),
                token(TOKEN_NAME, "IDENT"),
                token(SLASH, "/"),
                token(L_BRACKET, "["),
                token(CHAR, "A"),
                token(DASH, "-"),
                token(CHAR, "Z"),
                token(R_BRACKET, "]"),
                token(PLUS, "+"),
                token(SLASH, "/"),
                token(R_BRACE, "}")
        );
        assertEqualTokens(expected, tokens);
    }

    private void assertEqualTokens(List<LexToken> expected, List<LexToken> actual) {
        if (actual.size() != expected.size()) {
            Assert.fail("Expected " + expected.size() + " tokens but found " + actual.size());
        }
        Iterator<LexToken> actualIterator = actual.iterator();
        int index = 0;
        for (LexToken expectedToken : expected) {
            LexToken actualToken = actualIterator.next();
            if (expectedToken.getTokenType() != actualToken.getTokenType()) {
                Assert.fail("Expected type " + expectedToken.getTokenType() + " but found " +
                        actualToken.getTokenType() + " at index " + index);
            }
            if (!expectedToken.getText().equals(actualToken.getText())) {
                Assert.fail("Expected text " + expectedToken.getTokenType() + " but found " +
                        actualToken.getTokenType() + " at index " + index);
            }
            index++;
        }
    }

    private static LexToken token(LexToken.Type type, String text) {
        return new LexToken(type, 0, text);
    }
}
