package com.lazornet.grammar.lazorlex.lex;

import com.lazornet.grammar.lex.lexercompiler.LexerCompiler;
import com.lazornet.grammar.lex.lexercompiler.LexerDefinition;
import com.lazornet.grammar.lex.lexercompiler.LexerRule;
import com.lazornet.grammar.lex.lexercompiler.LexerState;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class LexParserTest {
    @Test
    public void noStates() throws LexParseException {
        List<LexToken> tokens = LexLexer.lexAllTokens("" +
                        "IDENTIFIER /[A-Z][a-z]*/ \n" +
                        "VAL \"val\" \n"
                        );

        LexerDefinition lexerDefinition = LexerCompiler.compile(LexParser.parse(tokens));

        assertEquals(1, lexerDefinition.getStates().size());
        LexerState defaultState = lexerDefinition.getStates().get(0);
        assertEquals("default", defaultState.getName());
        assertEquals(2, defaultState.getRules().size());
        assertLexerRule(defaultState.getRules().get(0), "IDENTIFIER", 0, "[A-Z][a-z]*", null, null);
        assertLexerRule(defaultState.getRules().get(1), "VAL", 1, null, "val", null);
    }

    @Test
    public void test() throws LexParseException {
        List<LexToken> tokens = LexLexer.lexAllTokens("" +
                "init {" +
                "    VAL \"val\"" +
                "    QUOTE /\"/ -> text_literal" +
                "}" +
                "text_literal {" +
                "    TEXT  /~\"+/" +
                "    QUOTE /\"/ -> init" +
                "}");

        LexerDefinition lexerDefinition = LexerCompiler.compile(LexParser.parse(tokens));

        assertEquals(2, lexerDefinition.getStates().size());

        LexerState initState = lexerDefinition.getStates().get(0);
        assertEquals("init", initState.getName());
        assertEquals(2, initState.getRules().size());
        assertLexerRule(initState.getRules().get(0), "VAL", 0, null, "val", null);
        assertLexerRule(initState.getRules().get(1), "QUOTE", 1, "\"", null, "text_literal");

        LexerState textLiteralState = lexerDefinition.getStates().get(1);
        assertEquals("text_literal", textLiteralState.getName());
        assertEquals(2, textLiteralState.getRules().size());
        assertLexerRule(textLiteralState.getRules().get(0), "TEXT", 2, "~\"+", null, null);
        assertLexerRule(textLiteralState.getRules().get(1), "QUOTE", 3, "\"", null, "init");
    }

    private void assertLexerRule(LexerRule lexerRule, String name, int index, String regex, String exactText, String nextState) {
        assertEquals(name, lexerRule.getTokenName());
        assertEquals(index, lexerRule.getIndex());
        assertEquals(exactText, lexerRule.getExactText());
        if (lexerRule.getRegexDef() == null) {
            if (regex != null) throw new AssertionError("Expected regex " + regex + " but got null");
        } else {
            assertEquals(regex, lexerRule.getRegexDef().decompile());
        }
        assertEquals(nextState, lexerRule.getNextState());
    }
}
