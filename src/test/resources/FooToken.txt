package com.lazornet.grammar.lex.lexerparser;

public class FooToken {
    public enum Type {CHAR, DASH, DOT, DOUBLE_QUOTE, ESC_BACKSLASH, ESC_CR, ESC_LF, ESC_TAB, L_BRACE, L_BRACKET,
        MACRO_NAME, PLUS, R_ARROW, R_BRACE, R_BRACKET, SLASH, STAR, STATE_NAME, TEXT, TILDE, TOKEN_NAME, _WHITESPACE}

    private final Type tokenType;
    private final int tokenStart;
    private final String text;

    public FooToken(Type tokenType, int tokenStart, String text) {
        this.tokenType = tokenType;
        this.tokenStart = tokenStart;
        this.text = text;
    }

    public Type getTokenType() {
        return tokenType;
    }

    public int getTokenStart() {
        return tokenStart;
    }

    public String getText() {
        return text;
    }
}
