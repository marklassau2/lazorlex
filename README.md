# README #

Lazorlex is a Maven plugin that generates Parsers and Lexers.

### What is this repository for? ###

* Generating a Java code Lexer from a .lex definition file.
* Generating a Java code Parser from a .grm definition file.
* Version: beta

### How do I get set up? ###

* This is a standard Maven plugin project.
* Use `mvn install` to install a new version of the plugin locally that you can then consume in another maven project.

### Contribution guidelines ###

* Tests are good ... mkay?
* Raise a pull request.

### Who do I talk to? ###

* Raise an issue or a pull request